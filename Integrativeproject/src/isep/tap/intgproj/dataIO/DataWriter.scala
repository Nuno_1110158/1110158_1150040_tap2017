package isep.tap.intgproj.dataIO

import isep.tap.intgproj.prodPlan.Schedule

trait DataWriter {
  
  
  def writeIt(fname: String,schedule:Schedule):Option[String]
  
}