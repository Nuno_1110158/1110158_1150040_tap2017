package isep.tap.ca3.dataReader

import isep.tap.ca3.model
import java.awt.WaitDispatchSupport
import com.github.tototoshi.csv.CSVReader
import isep.tap.ca3.model.Recipe


class DataReaderImplCSV extends DataReader{
  
  
  override def read(fname: String): RecipeList = {
    
    val whatFile:String = System.getProperty("user.dir")+"/files/"+fname
    
    val reader=CSVReader.open(whatFile) 
    //"drop(1) - remover nome das colunas
    val returRecipe=reader.all.drop(1).map{      
      case List(name,contry)=> Recipe(name,contry)
    }
    
     reader.close()
     
     new RecipeList(returRecipe)
  
  }
  
}