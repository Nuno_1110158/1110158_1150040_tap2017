package isep.tap.ca3.tests

import java.util.Calendar
import org.scalatest.FunSuite
import isep.tap.ca3.model.Recipe
import isep.tap.ca3.dataReader.DataReaderImplJSON
import isep.tap.ca3.logger.TimeTagLogger
import isep.tap.ca3.logger.TimeTagLogger

class InitialTest extends FunSuite {
  
  test("Exercise 1 initial conditions...") {
    val dataReader = new DataReaderImplJSON
    
    val expected = List(Recipe("Portuguese Green Soup","Portugal"), Recipe("Grilled Sardines","Portugal"), Recipe("Salted Cod with Cream","Portugal"))
    val other_expected =List(Recipe("Sirloin Steak","United Kingdom"),Recipe("Vichyssoise","France"),Recipe("Fondue","Switzerland"))

    assert(dataReader.readDataFast("recipe.json").lr===expected)
    assert(dataReader.readDataSlow("other_recipe.json").lr===other_expected)
  }  
  
  test("Exercise 2 initial conditions...") {
    val log = new TimeTagLogger
    
    val msg = "Dumy message!"
    val tt = Calendar.getInstance().getTime().toString() // Current date/time
    
    log.log(msg, tt)
    
    assert(log.top()===(msg,tt))    
  }  
  
}