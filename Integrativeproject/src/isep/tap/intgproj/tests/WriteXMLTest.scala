package isep.tap.intgproj.tests

import scala.xml.XML
import org.scalatest.FunSuite
import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._
import isep.tap.intgproj.prodPlan._

class WriteXMLTest extends FunSuite {
  
  val physResources1=PhysicalResource("PRS_1","PRST 1")
  val physResources2=PhysicalResource("PRS_2","PRST 2")
  val humanResource1=HumanResource("HRS_1","Antonio",List("PRST 1","PRST 2"))
  val humanResource2=HumanResource("HRS_2","Maria",List("PRST 1","PRST 2"))
  val taskschedule=TasksSchedule("ORD_1",1,"TSK_1",0,100,List(physResources1,physResources2),List(humanResource1,humanResource2))
 
  val  theSchedule=Schedule(List(taskschedule))
  
  val datawriterxml=new DataWriterImpXML
  
  test("Teste 1 Ficheiro guardado sem erro...") {
    
      
      
      val writeTheFile=datawriterxml.writeIt("fileTeste.xml", theSchedule)
       
      assert(writeTheFile.get==="Ficheiro Guardado")
      
  }
  
   test("Teste 2 Ficheiro não foi guardado, com erro IO...") {
          
        val writeTheFile=datawriterxml.writeIt("lixo/fileTeste", theSchedule)
         
        assert(writeTheFile===None)
      
  }
  
   
  test("Teste 3 Ficheiro foi guardado, sem dados...") {
    
      val  theScheduleVazio=Schedule(List())
           
      val writeTheFile=datawriterxml.noDatawriteIt("vazioTeste.xml",theScheduleVazio)
      
      assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 4 Ficheiro foi guardado, sem dados - comerro IO...") {
    
      val  theScheduleVazio=Schedule(List())
           
      val writeTheFile=datawriterxml.noDatawriteIt("lixo/vazioTeste.xml",theScheduleVazio)
      
      assert(writeTheFile===None)
  }
  
  test("Teste 5 Teste Completo MileStone 1") {
    
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v1.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
    //val schedule:Schedule = xml \ "Schedule";
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    //assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste6.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  /*
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   * ALGORITMO ALTERNATIVO MILESTONE 2
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   */
  
  test("Teste 6 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v1.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v1.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste7.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 7 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v2.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v2.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste8.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 8 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v3.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v3.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste9.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 9 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v4.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v4.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste10.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 10 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v5.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v5.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste11.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  //Verifica se o novo algoritmo é tão ou mais rápido do que o da milestone 1
  test("Teste 16 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val planProductionM2 = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsListM2 = planProductionM2.orderIntoSchedule(orderResources,0)
    val theScheduleM2 = Schedule(tsListM2);
    
    assert(theScheduleM2.taskScheduleList.last.end <= theSchedule.taskScheduleList.last.end)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste17.xml", theScheduleM2)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  //Verifica se para um ficheiro sem PhysicalResources, a lista de TasksSchedule devolvida é vazia
  test("Teste 18 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip2_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val planProductionM2 = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsListM2 = planProductionM2.orderIntoSchedule(orderResources,0)
    val theScheduleM2 = Schedule(tsListM2);
    
    assert(theScheduleM2.taskScheduleList == List())
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste19.xml", theScheduleM2)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  //Verifica se para um ficheiro sem HumanResources, a lista de TasksSchedule devolvida é vazia
  test("Teste 20 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip3_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val planProductionM2 = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsListM2 = planProductionM2.orderIntoSchedule(orderResources,0)
    val theScheduleM2 = Schedule(tsListM2);
    
    assert(theScheduleM2.taskScheduleList == List())
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste21.xml", theScheduleM2)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  /*
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   * ALGORITMO PRINCIPAL MILESTONE 2
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   */
  
  test("Teste 11 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v1.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v1.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in_ok_3p_2t_v1.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)
    
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida === tasksSchedule)
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste12.xml")
  }
      
  test("Teste 12 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v2.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v2.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in_ok_3p_2t_v2.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)
    
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida === tasksSchedule)
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste13.xml")
  }
        
  test("Teste 13 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v3.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v3.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in_ok_3p_2t_v3.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)
    
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida === tasksSchedule)
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste14.xml")
  }
          
  test("Teste 14 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v4.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v4.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in_ok_3p_2t_v4.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)
    
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida === tasksSchedule)
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste15.xml")
  }
  
  test("Teste 15 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_3p_2t_v5.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_3p_2t_v5.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in_ok_3p_2t_v5.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)
    
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida === tasksSchedule)
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste16.xml")
  }
  
  //Verifica se o novo algoritmo é tão ou mais rápido do que o da milestone 1
  test("Teste 17 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)  
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida.last.end <= theSchedule.taskScheduleList.last.end)
    
    val datawriterxml=new DataWriterImpXML
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste18.xml")
  }
  
  //Verifica se para um ficheiro sem PhysicalResources, a lista de TasksSchedule devolvida é vazia
  test("Teste 19 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip2_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip2_2017_in.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)  
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida == List())
    
    val datawriterxml=new DataWriterImpXML
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste20.xml")
  }
  
  //Verifica se para um ficheiro sem HumanResources, a lista de TasksSchedule devolvida é vazia
  test("Teste 21 Milestone2") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip3_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip3_2017_in.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)  
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    assert(tasksscheduleSaida == List())
    
    val datawriterxml=new DataWriterImpXML
    SchedulerMilestone2.criarTasksScheduleToFile("fileTeste22.xml")
  }
  
  /*
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   * ALGORITMO ALTERNATIVO MILESTONE 3
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   */
  
  test("Teste 22 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v1.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v1.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone3(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,Nil,Nil,0,0,0,0,Nil,Nil,Nil,true);
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste23.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 23 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v2.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v2.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone3(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,Nil,Nil,0,0,0,0,Nil,Nil,Nil,true);
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste24.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 24 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v3.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v3.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone3(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,Nil,Nil,0,0,0,0,Nil,Nil,Nil,true);
    val theSchedule = Schedule(tsList);
    //assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste25.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 25 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v4.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v4.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone3(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,Nil,Nil,0,0,0,0,Nil,Nil,Nil,true);
    val theSchedule = Schedule(tsList);
    assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste26.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 26 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v5.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v5.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);
    
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
  
    val planProduction = PlanProductionMilestone3(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,Nil,Nil,0,0,0,0,Nil,Nil,Nil,true);
    val theSchedule = Schedule(tsList);
    //assert(theSchedule.taskScheduleList === tasksSchedule)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste27.xml", theSchedule)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  //Verifica se o novo algoritmo é tão ou mais rápido do que o da milestone 1 e da milestone2
  test("Teste 27 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val planProductionM2 = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsListM2 = planProductionM2.orderIntoSchedule(orderResources,0)
    val theScheduleM2 = Schedule(tsListM2);
    
    val planProductionM3 = PlanProductionMilestone3(physicalResources,taskResources,humanResources,productResources);
    val tsListM3 = planProductionM3.orderIntoSchedule(orderResources,Nil,Nil,0,0,0,0,Nil,Nil,Nil,true);
    val theScheduleM3 = Schedule(tsListM3);
    
    assert(theScheduleM3.taskScheduleList.last.end <= theScheduleM2.taskScheduleList.last.end && theScheduleM3.taskScheduleList.last.end <= theSchedule.taskScheduleList.last.end)
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste28.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  /*
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   * ALGORITMO PRINCIPAL MILESTONE 3
   * ///////////////////////////////////////////////////////////////////////////////////////////////////
   */
  
  test("Teste 28 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v1.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v1.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    val fxml="ip_2017_in_ok_2o_3p_2t_v1.xml"  
    
    val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
    val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
    val definirEscalonamento= EscalonamentoTasksPlanMil3()

    val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
    val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
    val tasksPodemSerExecParalelo=definirEscalonamento.escalonarTasksMil3(objectoParaEscalonar)
   
    val listaOrdersProdsTasksExecutar=tasksPodemSerExecParalelo.listTaskScheResultado
    
    assert(listaOrdersProdsTasksExecutar === tasksSchedule)
    
    val theScheduleM3 = Schedule(listaOrdersProdsTasksExecutar);
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste29.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 29 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v2.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v2.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    val fxml="ip_2017_in_ok_2o_3p_2t_v2.xml"  
    
    val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
    val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
    val definirEscalonamento= EscalonamentoTasksPlanMil3()

    val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
    val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
    val tasksPodemSerExecParalelo=definirEscalonamento.escalonarTasksMil3(objectoParaEscalonar)
   
    val listaOrdersProdsTasksExecutar=tasksPodemSerExecParalelo.listTaskScheResultado
    
    assert(listaOrdersProdsTasksExecutar === tasksSchedule)
    
    val theScheduleM3 = Schedule(listaOrdersProdsTasksExecutar);
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste30.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 30 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v3.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v3.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    val fxml="ip_2017_in_ok_2o_3p_2t_v3.xml"  
    
    val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
    val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
    val definirEscalonamento= EscalonamentoTasksPlanMil3()

    val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
    val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
    val tasksPodemSerExecParalelo=definirEscalonamento.escalonarTasksMil3(objectoParaEscalonar)
   
    val listaOrdersProdsTasksExecutar=tasksPodemSerExecParalelo.listTaskScheResultado
    
    assert(listaOrdersProdsTasksExecutar === tasksSchedule)
    
    val theScheduleM3 = Schedule(listaOrdersProdsTasksExecutar);
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste31.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 31 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v4.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v4.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    val fxml="ip_2017_in_ok_2o_3p_2t_v4.xml"  
    
    val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
    val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
    val definirEscalonamento= EscalonamentoTasksPlanMil3()

    val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
    val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
    val tasksPodemSerExecParalelo=definirEscalonamento.escalonarTasksMil3(objectoParaEscalonar)
   
    val listaOrdersProdsTasksExecutar=tasksPodemSerExecParalelo.listTaskScheResultado
    
    assert(listaOrdersProdsTasksExecutar === tasksSchedule)
    
    val theScheduleM3 = Schedule(listaOrdersProdsTasksExecutar);
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste32.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  test("Teste 32 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_out_ok_2o_3p_2t_v5.xml"
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in_ok_2o_3p_2t_v5.xml"
    val xmlOut=XML.loadFile(whatFile)
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);  
    val tasksSchedule = datareaderImpXML.tasksScheduleResourceList(xmlOut, physicalResources, humanResources);
     
    val fxml="ip_2017_in_ok_2o_3p_2t_v5.xml"  
    
    val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
    val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
    val definirEscalonamento= EscalonamentoTasksPlanMil3()

    val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
    val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
    val tasksPodemSerExecParalelo=definirEscalonamento.escalonarTasksMil3(objectoParaEscalonar)
   
    val listaOrdersProdsTasksExecutar=tasksPodemSerExecParalelo.listTaskScheResultado
    
    assert(listaOrdersProdsTasksExecutar === tasksSchedule)
    
    val theScheduleM3 = Schedule(listaOrdersProdsTasksExecutar);
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste33.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
  
  //Verifica se o novo algoritmo é tão ou mais rápido do que o da milestone 1 e da milestone2
  test("Teste 33 Milestone3") {
    val datareaderImpXML=new DataReaderImpXML
    val inFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in.xml"
    val xml = XML.loadFile(inFile)
       
    val physicalResources = datareaderImpXML.physicalResourcesList(xml);
    val taskResources = datareaderImpXML.taskResourceList(xml);
    val humanResources = datareaderImpXML.humanResourceList(xml);
    val productResources = datareaderImpXML.productResourceList(xml);
    val orderResources = datareaderImpXML.ordersResourceList(xml);  
  
    val planProduction = PlanProduction(physicalResources,taskResources,humanResources,productResources);
    val tsList = planProduction.orderIntoSchedule(orderResources,0)
    val theSchedule = Schedule(tsList);
    
    val planProductionM2 = PlanProductionMilestone2(physicalResources,taskResources,humanResources,productResources);
    val tsListM2 = planProductionM2.orderIntoSchedule(orderResources,0)
    val theScheduleM2 = Schedule(tsListM2);
    
    val SchedulerMilestone2 = DoTaskScheduleByOrder("ip_2017_in.xml");
    val multipleProdTaskPlan=MultipleProdTaskPlan(SchedulerMilestone2.produzir)  
    val tasksscheduleSaida= multipleProdTaskPlan.ordemsToTaskSchedule(SchedulerMilestone2.asOrdens)
    
    val fxml="ip_2017_in.xml"  
    
    val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
    val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
    val definirEscalonamento= EscalonamentoTasksPlanMil3()

    val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
    val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
    val tasksPodemSerExecParalelo=definirEscalonamento.escalonarTasksMil3(objectoParaEscalonar)
   
    val listaOrdersProdsTasksExecutar=tasksPodemSerExecParalelo.listTaskScheResultado
    
    assert(listaOrdersProdsTasksExecutar.last.end <= theScheduleM2.taskScheduleList.last.end 
        && listaOrdersProdsTasksExecutar.last.end <= theSchedule.taskScheduleList.last.end
        && listaOrdersProdsTasksExecutar.last.end <= tasksscheduleSaida.last.end)
    
    val theScheduleM3 = Schedule(listaOrdersProdsTasksExecutar);
    
    val datawriterxml=new DataWriterImpXML
    val writeTheFile=datawriterxml.writeIt("fileTeste34.xml", theScheduleM3)      
    assert(writeTheFile.get==="Ficheiro Guardado")
  }
}