package isep.tap.intgproj.prodPlan

import scala.xml.XML

case class Schedule(taskScheduleList:List[TasksSchedule]){
  
def toXML:xml.Node={
<Schedule xmlns="http://www.dei.isep.ipp.pt/ip_2017" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/ip_2017 ip_2017_out.xsd">
	{taskScheduleList.map(tkS=>tkS.toXML)}
</Schedule>
 			
 			}

//se não houver dados para o ficheiro XML
def noDataToXML:xml.Node={
  <Schedule xmlns="http://www.dei.isep.ipp.pt/ip_2017" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/ip_2017 ip_2017_out.xsd"/>
}

def noTaskScheduleList:Boolean={
  
  this.taskScheduleList.isEmpty
  
}
   			
}
  

  