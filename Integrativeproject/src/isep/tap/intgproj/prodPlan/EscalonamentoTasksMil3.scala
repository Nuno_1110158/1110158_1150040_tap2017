package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._

case class EscalonamentoTasksMil3(fname:String) {
  
    val dataReadImpXML= new DataReaderImpXML
    
    val escreverTasksSchedule=new DataWriterImpXML
    
    //ler o ficheiro xml
    val ordemDeProducao=dataReadImpXML.read(fname)
    
    //plano de escalonamento
    val escalonamentoPlan= EscalonamentoTasksPlanMil3()
    
    //OrdemProduto que podem executar
    val objectsPlanMil3=ObjectsPlanMil3(produzir)
    
    
      def criarTasksScheduleToFile(fnameOut:String):Option[String]={
      
      if(existemDadosFromFile){ 
          val ordProdutosQuePodemSerProduzidos=objectsPlanMil3.ordersProdsTasksExecutar(asOrdens)
          
          val objectoParaescalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,osRecursos)	
          
          val novoObjectoParaescalonar= escalonamentoPlan.escalonarTasksMil3(objectoParaescalonar)
          
          val tasksschedule=novoObjectoParaescalonar.listTaskScheResultado
        
          escreverTasksSchedule.writeIt(fnameOut, Schedule(tasksschedule))
        
      }else{
        
        escreverTasksSchedule.writeIt(fnameOut, Schedule(Nil))
      }
            
    }
    
    /**
     * Se existirem dados do ficheiro devolve "Production" com os dados
     * Se existiu um erro qualquer devolve "Production" vazia
     * 
     * @return devolve uma "Production"
     */
    def produzir:Production={
      
        if(existemDadosFromFile) ordemDeProducao.get
        else Production(Nil,Nil,Nil,Nil,Nil)          
    }
    
    
    
    /**
     * Verifica se existem dados do ficheiro, de devolve true ou false
     * 
     * @return devolve true ou false
     */
    def existemDadosFromFile:Boolean={
      
        if(ordemDeProducao==None) false else true
    }
    
       /**
     * Devolve as tasks ou lista vazia
     * 
     * @return devolve lista de tasks
     */
    def asTasks:List[Task]={
      
      if(existemDadosFromFile) ordemDeProducao.get.tasks else Nil
    }
    
    
    /**
     * Devolve os recursos fisicos e humanos ou lista vazia
     * 
     * @return devolve lista de recursos fisicos e humanos
     */
    def osRecursos:Recursos={
      
      if(existemDadosFromFile) Recursos(ordemDeProducao.get.fResources,ordemDeProducao.get.hResources) 
      else Recursos(Nil,Nil)      
        
    } 
    
         /**
     * Devolve as ordens ou lista vazia
     * 
     * @return devolve lista de ordens
     */
    def asOrdens:List[Order]={
      
      if(existemDadosFromFile) ordemDeProducao.get.orders else Nil
      
    }
  
}