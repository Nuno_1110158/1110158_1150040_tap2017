package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.model._

case class Recursos(phys:List[PhysicalResource], hums:List[HumanResource])