package isep.tap.intgproj.prodPlan


import isep.tap.intgproj.model._

case class OrderTaskPlanPlan() {
  
  //--------- Instante zero--------------------------------------------------------------------------------------
  
    /**
   * Recebe uma lista de "OrdemProduto" por cada ordem ou de varias ordens para serem executadas
   * 
   * @param lista de "OrdemProduto" para executar
   * @return lista de "TasksSchedule" que depois vai ser usada para o XML_Output
   */
  def instanteZero(opt:List[OrdemProduto],recs:Recursos,start:Int,end:Int):List[TasksSchedule]= {


			def iniciar(opts:List[OrdemProduto],recss:Recursos,starts:Int,ends:Int):List[TasksSchedule]= opts match{
			
					case Nil=>Nil
					
					case tklist=>
												//val listatasksExecutar=tasksPodemSerExec(tklist, recss,List()) //lista de tasks a serem executadas ao mesmo tempo
												
					  					val listatasksExecutar=tasksPodemSerExec(tklist, recss)
					  
												//println("Lista a executar - "+listatasksExecutar.toString())
												
												//tratar erro - UnsupportedOperationException: empty.maxBy (pode aparecer no meio uma lista vazia)
												if(listatasksExecutar.length>0){
												val endMax=listatasksExecutar.maxBy ({ opx => opx.prod.tasks.head.time }).prod.tasks.head.time //maximo para o end
												
												// três operações a serem feitos para a lista que vai prosseguir. 
												//1-remover a task da lista de tasks dp produto
												//2-tenho que substituir na lista total os produtos/tasks que já foram produzidos
												//3-remover os produtos com a lista de tasks vazia (o produto está produzido)
												val removeTaskDeProduto=removerTaskDoProduto(listatasksExecutar)  //remover as tasks já executadas
												
												//println("Remover tasks do produto - "+ removeTaskDeProduto.toString())
												//tenho que substituir na lista total os produtos/tasks que já foram produzidos
												
												val substituirProdutosNaListaTotal=substituirProdutos(removeTaskDeProduto,tklist)
												
												//println("substituir produto - "+ substituirProdutosNaListaTotal.toString())
												val removerProdutoJaProduzido=removerDaListaProdsSemTasks(substituirProdutosNaListaTotal)  //produto com lista de task vazia
												 
												//println("lista redefinida - "+ removerProdutoJaProduzido.toString())
												dameTasksSchedules(listatasksExecutar, starts, starts+endMax, recss):::iniciar(removerProdutoJaProduzido,recss,starts+endMax,endMax)  //aqui o end não é importante porque é recalculado
												
												}else{
												  
												  //acho que nunca chega aqui
												  iniciar(Nil,recss,starts,end)  //aqui o end não é importante porque é recalculado
												  
												}
			}
			
			iniciar(opt,recs,start,end)
  }
  
  
  
    /**
   * Auxilia a função "instanteZero" (anterior)
   * Recebe uma lista de "OrdemProduto" que estão prontos a executar
   * escolhe quais podem ser executadas ao mesmo tempo
   * devolve uma lista de "OrdemProduto" (tasks) 
   * 
   * @param lista de "OrdemProduto" que estão para executar
   * @return lista de "OrdemProduto" executadas ao mesmo tempo
   */
  def tasksPodemSerExec(opt:List[OrdemProduto],recs:Recursos):List[OrdemProduto]= opt match{
  
  		case Nil=>Nil
  		case tk::Nil=> val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		          
  		        if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)) {
  		  
  							  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //println("Recursos reduzidos - "+ recursosRed.toString())
  							  tk::tasksPodemSerExec(Nil,recursosRed)
  							  
  							}else{ Nil}
  							
  		case tk::tks=>val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		  
  		          if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)) {
  		              
  		            val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //println("Recursos reduzidos - "+ recursosRed.toString())
  							  tk::tasksPodemSerExec(tks,recursosRed)
  						
  								
  							}else{ tasksPodemSerExec(tks,recs)}

  } 
  
    /**
   * Auxilia a função "instanteZero"
   * Recebe uma lista de "OrdemProduto" para devolver uma lista de "TasksSchedule" da lista de entrada.
   * A lista de entrada contém as "OrdemProduto" que podem ser executadas ao mesmo tempo
   * 
   * @param lista de "OrdemProduto" que podem ser executadas ao mesmo tempo
   * @return devolve lista de "TasksSchedule"
   */
  def dameTasksSchedules(opt:List[OrdemProduto],start:Int,end:Int,recs:Recursos):List[TasksSchedule]= opt match{
  
  		case Nil=>Nil
  		case tk::Nil=> 
  		               val taskphyresources=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task  									 
  									 val taskhumanresources= tk.prod.tasks.head.numRechuman(taskphyresources, recs.hums) //Lista de recursos humanos para os recursos fisicos desta task
  										
  	                //reduzir a lista de recursos
    								val phyRecsAtribuidosRed=removerDaLista(taskphyresources, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
    								val humRecsAtribuidosRed=removerDaLista(taskhumanresources, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  		               
  		               val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  										
  										TasksSchedule(tk.ordId,tk.prod.nump,tk.prod.tasks.head.id,start,tk.prod.tasks.head.time+start,
  										taskphyresources,taskhumanresources)::dameTasksSchedules(Nil,start,end,recursosRed)
  	
  	  case tk::tks=> val taskphyresources=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  									 val taskhumanresources= tk.prod.tasks.head.numRechuman(taskphyresources, recs.hums) //Lista de recursos humanos para os recursos fisicos desta task
  										
                      //reduzir a lista de recursos
      								val phyRecsAtribuidosRed=removerDaLista(taskphyresources, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
      								val humRecsAtribuidosRed=removerDaLista(taskhumanresources, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
    		               
  		                val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  									 
  									 
  										TasksSchedule(tk.ordId,tk.prod.nump,tk.prod.tasks.head.id,start,tk.prod.tasks.head.time+start,
  										taskphyresources,taskhumanresources)::dameTasksSchedules(tks,start,end,recursosRed)
  
  }
  
  //remover task da lista de tasks

  def removerTaskDoProduto(ordProExec:List[OrdemProduto]):List[OrdemProduto]= ordProExec match {
  
  		case Nil=>Nil
  		
  		case ts::Nil=>
  									val removetaskHead1=ts.prod.tasks.tail   // tail da lista de tasks
  									
  									val removetaskHead2=ts.prod.copy(tasks=removetaskHead1)  //copiar o produto com novalista de tasks
  									
  									val removetaskHead3=ts.copy( prod=removetaskHead2)  //copiar o "OrdemProduto" com novo produto
  									
  									removetaskHead3::removerTaskDoProduto(Nil)   //Novo "OrdemProduto" sem uma head task
  									
  	 case ts::tks=>
  									val removetaskHead1=ts.prod.tasks.tail   // tail da lista de tasks
  									
  									val removetaskHead2=ts.prod.copy(tasks=removetaskHead1)  //copiar o produto com novalista de tasks
  									
  									val removetaskHead3=ts.copy( prod=removetaskHead2)  //copiar o "OrdemProduto" com novo produto
  									
  									removetaskHead3::removerTaskDoProduto(tks)   //Novo "OrdemProduto" sem uma head task
  
  } 

   def substituirProdutos(list1:List[OrdemProduto],listTotal:List[OrdemProduto]):List[OrdemProduto]={
 
 				
 				 def substituirProduto(list1:List[OrdemProduto],listTotal:List[OrdemProduto]):List[OrdemProduto]= list1 match{
 				 
 				 		case Nil=>listTotal
 				 		case pr::Nil=> val ppp=substituirUmProduto(pr,listTotal)
 				 									substituirProduto(Nil,ppp)
 				 		case pr::prs=> val ppp=substituirUmProduto(pr,listTotal)
 				 									substituirProduto(prs,ppp)
 				 }
 
 			substituirProduto(list1,listTotal)
   } 
   
    def substituirUmProduto(orderprod:OrdemProduto,listTotal:List[OrdemProduto]):List[OrdemProduto]={
 
 				listTotal.map{ p=> if(p.prod.nump==orderprod.prod.nump) orderprod else p }
 } 
    
  //----funções auxiliares de varias funções----------------------------------------------------------------------------------
  
    /**
     * remove da "listaTotal" os elementos da "subLista", de qualquer tipo
     * é usada em várias funções
     */
    def removerDaLista(subLista:List[Any],listaTotal:List[Any]):List[Any]={
    
    		listaTotal.filterNot(subLista.contains(_))
    }   
  
    
    def removerDaListaProdsSemTasks(subLista:List[OrdemProduto]):List[OrdemProduto]={
    
    		subLista.filterNot { x => x.prod.tasks==Nil }
    }
    
    
  
}