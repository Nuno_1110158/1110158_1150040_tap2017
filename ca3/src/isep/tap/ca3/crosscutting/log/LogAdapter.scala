package isep.tap.ca3.crosscutting.log

import isep.tap.ca3.logger.TimeTagLogger
import java.util.Calendar

/*
 * DONE: create the LogAdapter object
 */

package object LogAdapter {  
 implicit class TimeStampLogger(TimeTag: TimeTagLogger) extends Log {
  def log(message: String) = TimeTag.log(message,Calendar.getInstance().getTime().toString())
  def top(): (String, String) = TimeTag.top()
  }
}