package isep.tap.intgproj.dataIO

import isep.tap.intgproj.model._


case class Production(fResources:List[PhysicalResource],
                      tasks:List[Task],
                      hResources:List[HumanResource],
                      products:List[ProductToDo],
                      orders:List[Order]){
  
  def noDataToSave:Boolean={
      
    this.fResources.isEmpty && 
    this.tasks.isEmpty && 
    this.hResources.isEmpty && 
    this.products.isEmpty && 
    this.orders.isEmpty    
  }

}
  
                        
