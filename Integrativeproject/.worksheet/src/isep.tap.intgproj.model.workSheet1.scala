package isep.tap.intgproj.model

import scala.xml.XML
import isep.tap.intgproj.prodplan.TaskSchedule

object workSheet1 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(165); 
  println("Welcome to the Scala worksheet");$skip(62); 
  
  
  val physResources1=PhysicalResource("PRS_1","PRST 1");System.out.println("""physResources1  : isep.tap.intgproj.model.PhysicalResource = """ + $show(physResources1 ));$skip(56); 
  val physResources2=PhysicalResource("PRS_2","PRST 2");System.out.println("""physResources2  : isep.tap.intgproj.model.PhysicalResource = """ + $show(physResources2 ));$skip(81); 
  
  val humanResource1=HumanResource("HRS_1","Antonio",List("PRST 1","PRST 2"));System.out.println("""humanResource1  : isep.tap.intgproj.model.HumanResource = """ + $show(humanResource1 ));$skip(76); 
  val humanResource2=HumanResource("HRS_2","Maria",List("PRST 1","PRST 2"));System.out.println("""humanResource2  : isep.tap.intgproj.model.HumanResource = """ + $show(humanResource2 ));$skip(131); 
  
 val taskschedule=TaskSchedule("ORD_1",1,"TSK_1",0,100,List(physResources1,physResources2),List(humanResource1,humanResource2));System.out.println("""taskschedule  : <error> = """ + $show(taskschedule ))}
 
 
 //----------------------------------------------------------------------------------------------------------------------------
 
 // val xml=XML.loadFile("D:/ISEP/ISEP_Mestrado/Semestre_2/TAP/CA3/Integrativeproject/files/ip2_2017_in.xml")
   

                                       
  //----------------------------------------------------------------------------------------------------------------------
 /*
       def nodeResource(node:scala.xml.Node,prop:String):String={
  
  			(node \ prop).text
  
  	}
*/
//------------------------------------------------------------------------------
/*
  val ProductsTD=xml \ "Products"

   def processtskref(node:scala.xml.Node):String={
  
  		(node \"@tskref").text
  
  }
  
      def  ProductsfromXML(node:scala.xml.Node):ProductToDo={
  
  		val p_id=(node \"@id").text
  		val p_nome=(node \ "@name").text
  		val p_processtskref=(node \"Process").map(processtskref).toList
  		ProductToDo(p_id,p_nome,p_processtskref)
  }
  
val productTaskref=(ProductsTD \"Product").map(ProductsfromXML).toList
 */

//-------------------------------------------------------------------------------------------------------------
  /*
  val human=xml \ "HumanResources"
 
  

  def hResource(node:scala.xml.Node):String={
  
  		(node \"@type").text
  
  }


   def humanRfromXML(node:scala.xml.Node):HumanResource={
  
  		val p_id=(node \"@id").text
  		val p_nome=(node \ "@name").text
  		val p_handlesType=(node \"Handles").map(hResource).toList
  		HumanResource(p_id,p_nome,p_handlesType)
  }
  
   val humanRec=(human \"Human").map(humanRfromXML).toList
*/

//-------------------------------------------------------------------------------------
/*
  def PhysicalResourcesList(elem:scala.xml.Elem):List[PhysicalResource]={
    
      val phResouces=elem \ "PhysicalResources"
      
            def phyResoucesfromXML(node:scala.xml.Node):PhysicalResource={
  
            		val p_id=(node \"@id").text
            		val p_type=(node \ "@type").text
          
            		PhysicalResource(p_id,p_type)
            }
      
       //Lista de "PhysicalResource"
     (phResouces \"Physical").map(phyResoucesfromXML).toList
      //phyResouces
    
  }

 		val phyResouces=PhysicalResourcesList(xml)
*/

//---------------------------------------------------------------------------------------------
  /*
   val humanRecs=xml \ "HumanResources"
   
      def humanRfromXML(node:scala.xml.Node):HumanResource={
  
  		val p_id=(node \"@id").text
  		val p_nome=(node \ "@name").text
  		val p_handlesType=(node \"Handles").map(n=>nodeResource(n,"@type")).toList
  		HumanResource(p_id,p_nome,p_handlesType)
  }
  
   val humanRec=(humanRecs \"Human").map(humanRfromXML).toList
   
  //--------------------------------------------------------------------------------------------------------------------
  
    
 
     val taskss=xml \ "Tasks"

  
    def phResource(node:scala.xml.Node,prop:String):String={
  
  			(node \ prop).text
  
  	}
  
    def taskfromXML(node:scala.xml.Node):Task={
  
  		val p_id=(node \"@id").text
  		val p_time=(node \ "@time").text.toInt
  		val p_phRec=(node \"PhysicalResource").map(n=>phResource(n,"@prstype")).toList
  		Task(p_id,p_time,p_phRec)
  }
  
  val TTasks=(taskss \"Task").map(taskfromXML).toList
 */
 
  
  
  //----------------------------------------------------------------------------------------------------------------------
  /*
    val phResouces=xml \ "PhysicalResources"
 
 
    def phResoucesfromXML(node:scala.xml.Node):PhysicalResource={
  
  		val p_id=(node \"@id").text
  		val p_type=(node \ "@type").text

  		PhysicalResource(p_id,p_type)
  }
 
  val phyResouces=(phResouces \"Physical").map(phResoucesfromXML).toList
  
 */
  //---------------------------------------------------------------------------------------------------------------------
  /*
  val orders=xml \ "Orders"
  
      def ordersfromXML(node:scala.xml.Node):Order={
  
  		val p_id=(node \"@id").text
  		val p_prdref=(node \ "@prdref").text
			val p_quant=(node \ "@quantity").text.toInt
  		Order(p_id,p_prdref,p_quant)
  }
  
  val orderss=(orders \"Order").map(ordersfromXML).toList
 
                                                  
 //----------------------------------------------------------------------------------------------
  
  val ProductsTD=xml \ "Products"
  
    

   def processtskref(node:scala.xml.Node):String={
  
  		(node \"@tskref").text
  
  }
 
  
    def  ProdsfromXML(node:scala.xml.Node):ProductToDo={
  
  		val p_id=(node \"@id").text
  		val p_nome=(node \ "@name").text
  		val p_processtskref=(node \"Process").map(processtskref).toList
  		ProductToDo(p_id,p_nome,p_processtskref)
  }
  
  val productTaskref=(ProductsTD \"Product").map(ProductsfromXML).toList
 
  
  
  
  */
  
  
}
