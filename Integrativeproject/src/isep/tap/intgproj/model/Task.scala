package isep.tap.intgproj.model

case class Task(id:String,time:Integer,physicRtype:List[String]){
  
    /**
     * Primeiro verifica se esta task tem os recursos fisicos necessarios e se existirem
     * verifica se para os recursos fisicos existem recursos humanos
     * 
     * @param phys Lista de Physical Resources disponiveis (total)
     * @param hums Lista de Human Resources existentes (total)
     * @return Retorna true se existirem os recursos e false se não existirem
     */
    def possoSerProduzida(phys:List[PhysicalResource], hums:List[HumanResource]):Boolean={
      
      
       val resulnumPhyRestask=numPhyRectask(phys)    //lista dos recursos fisicos disponiveis
       
       //println(id +"- physicals - "+resulnumPhyRestask.toString())
       val exitemrecurcosPHY=resulnumPhyRestask.length==this.physicRtype.length   //compara se existem suficientes recursos fisicos
              
      // println(id +"- physicals - "+exitemrecurcosPHY)
       
       if(exitemrecurcosPHY) {         
         
         val resulnumRechuman=numRechuman(resulnumPhyRestask,hums)   //Lista dos recursos humanos disponiveis
         
         //println(id +"- humans - "+resulnumRechuman.toString())

         //println(id +"- humans - "+testeHmasn) 
             
         resulnumPhyRestask.length==resulnumRechuman.length    //compara se existem suficientes recursos humanos
       
         
       }else{ false }
      
    }
    
    
   /**
    *  Para cada task quantos recusos fisicos existem?
    *  
    *  @param t esta Task
    *  @param py a lista de Physical Resources disponiveis (total)
    *  @return retorna a lista com os Physical Resources disponiveis para esta Task
    */
  def numPhyRectask(py:List[PhysicalResource]):List[PhysicalResource]={
  
  			//@tailrec
  			def task3(ls:List[String],pyy:List[PhysicalResource]):List[PhysicalResource]= ls match{
  			
  				case Nil=>Nil
  				case x::Nil=> val fff= giveMePhysicalResource(x,pyy)
  					if(fff!=null){
  							fff::task3(Nil,pyy)
  					}else{
  					
  							task3(Nil,pyy)
  					}
  				case x::xs=> val fff= giveMePhysicalResource(x,pyy)
  						if(fff!=null){
  						 //fff::task3(xs,pyy.filterNot { x =>x==fff })  //todos excepto fff
  						 fff::task3(xs,pyy)  //todos excepto fff - não está correcto a procura é sempre na lista total
  					}else{
  						task3(xs,pyy)
  						
  					}
  			}
  			
  			task3(this.physicRtype,py)
  }  
  
  
  /**
   * Auxiliar de "def numPhyRestask"
   * 
   * @param s type do Physical Resource
   * @param pyy a lista do Physical Resources que vai sendo diminuida dos já atribuidos
   * @return retorna um objecto do tipo PhysicalResource
   */
  def giveMePhysicalResource(s:String,pyy:List[PhysicalResource]):PhysicalResource={
  
  			pyy.find ({ x => x.typeR.equals(s) })getOrElse(null)
  } 

 // versão teste ---------------------------------------------------------------------------------
   /**
    *  Para cada task quantos recusos fisicos existem?
    *  
    *  @param t esta Task
    *  @param py a lista de Physical Resources disponiveis (total)
    *  @return retorna a lista com os Physical Resources disponiveis para esta Task
    */
  def numPhyRectask2(py:List[PhysicalResource]):List[PhysicalResource]={
  
  			//@tailrec
  			def task3(ls:List[String],pyy:List[PhysicalResource]):List[PhysicalResource]= ls match{
  			
  				case Nil=>Nil
  				case x::Nil=> val fff= giveMePhysicalResource(x,pyy)
  					if(fff!=null){
  							fff::task3(Nil,pyy)
  					}else{
  					
  							task3(Nil,pyy)
  					}
  				case x::xs=> val fff= giveMePhysicalResource(x,pyy)
  						if(fff!=null){
  						 fff::task3(xs,pyy.filterNot { x =>x==fff })  //todos excepto fff
  						 //fff::task3(xs,pyy)  //todos excepto fff - não está correcto a procura é sempre na lista total
  					}else{
  						task3(xs,pyy)
  						
  					}
  			}
  			
  			task3(this.physicRtype,py)
  }  
//-------------------------------------------------------------------------------------------------  
  /**
   * Exitem Human Resources para os Physical Resources desta task
   * 
   * @param py Lista de Physical Resources existentes (total)
   * @param hm lista de Human Resources existentes (total)
   * @return Lista de Human Resources encontrados
   */
  
  def numRechuman(py:List[PhysicalResource],hm:List[HumanResource]):List[HumanResource]={
			//@tailrec
			def phyhuman(py2:List[PhysicalResource],hm2:List[HumanResource]):List[HumanResource]= py2 match{
			
					case Nil => Nil
					case x::Nil => val hhh=giveMeHumanResource(x.typeR,hm2)
							if(hhh!=null){
							
									hhh::phyhuman(Nil,hm2)
							}else{
							
									phyhuman(Nil,hm2)
							}
				
				case x::xs =>val hhh=giveMeHumanResource(x.typeR,hm2)
				
							if(hhh!=null){
							
									hhh::phyhuman(xs,hm2.filterNot { x => x==hhh })   //todos excepto hhh- 
									//hhh::phyhuman(xs,hm2)
							}else{
							
									phyhuman(xs,hm2)
							}
				
			}
			
			
			phyhuman(py,hm)

}   
  
  /**
   * Auxiliar de def numRechuman
   * 
   * @param s tipo de Physical Resource que operam
   * @param hm3 Lista dos Human Resources, que diminui sem os que já foram atribuidos
   * @return retorna um objecto HumanResource
   */
  def giveMeHumanResource(s:String,hm3:List[HumanResource]):HumanResource={
  
  			hm3.find ({ x => existshuman(s,x.handeltype)}).getOrElse(null)
  } 
  
  /**
   * Auxiliar de def giveMeHumanResource
   * 
   * @param s tipo de Physical Resource
   * @param	ll Lista de Physical Resource que operam 
   * @return retorna true se o Human Resource opera esse Physical Resource false se não opera
   */
  //@tailrec
  def existshuman(s:String,ll:List[String]):Boolean= ll match{
 
 			case Nil=>false
 			case x::Nil if(x==s)=>true
 			case x::xs => if(x==s) true else existshuman(s,xs)
 			
 } 
  
  
  /**
   * se existe esta task na lista de tasks
   */
  def euExisto(tsk:List[Task]):Boolean={
    
    val existo=tsk.find { x => x.id==this.id }
    
    if(existo==None) false else true
    
    
  }
    
  
}
