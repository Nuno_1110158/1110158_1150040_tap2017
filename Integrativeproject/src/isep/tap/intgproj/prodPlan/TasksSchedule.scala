package isep.tap.intgproj.prodPlan

import scala.xml.XML
import isep.tap.intgproj.model._

case class TasksSchedule(order:String,productNum:Integer,task:String,start:Integer,end:Integer,
                        physResources:List[PhysicalResource],humanResources:List[HumanResource]) {
  
  def toXML:xml.Node = {
    //não sei porque no XML esta linha é escrita invertendo as propriedade
	<TaskSchedule end={end.toString()} start={start.toString()} task={task} productNumber={productNum.toString()} order={order}>
		<PhysicalResources>
			{physResources.map(phyresourceToXML)}
		</PhysicalResources>
		<HumanResources>
			{humanResources.map(humanresourcesToXML)}
		</HumanResources>
	</TaskSchedule>

  }
    
   def phyresourceToXML(phyresource:PhysicalResource):xml.Node ={
   
   			phyresource.toXML
   }
    
  
  def humanresourcesToXML(humanresource:HumanResource):xml.Node ={
  
  			humanresource.toXML
  
		}
    
  
}

//<TaskSchedule order={order} productNumber={productNum.toString()} task={task} start={start.toString()} end={end.toString()}>