package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._

/**
 * Trata de normalizar Ordem - > Produto -> Task
 * para a estrutura "OrdemProdutoTask"
 */
case class OrdemProdutoPlanProduction(producao:Production) {
     
    val myproducts= producao.products
    
    val mytasks= producao.tasks
    
//----------------------------------------------------------------------------------------------------------------------
    /**
     * devolve o conjunto dos recursos fisicos e humanos
     */
    
    def osRecursos:Recursos={
      
        Recursos(producao.fResources,producao.hResources)
    }

 //--------colocar no produto o objecto Task e não a referência------------------------------------------------------------   
   /**
    * Tranforma uma lista de "ProductToDo" (com refs. para tasks) em "ProdutoComTasks"
    * substitui refs, de tasks por objectos do tipo Task
    * 
    * @param lista de "ProductToDo"
    * @return devolve lista de "ProdutoComTasks"
    */
    //colocar no produto o objecto Task e não a referência
  def productTodoTOproduto(ptd:List[ProductToDo]):List[ProdutoComTasks]={
  
  			
  			def produtosComtasks(ptd:List[ProductToDo]):List[ProdutoComTasks]= ptd match{
  			
  					case Nil=>Nil
  					case x::Nil=>ProdutoComTasks(x.id,x.name,splitprodutoNum2(x.name),refstaskTotask(x.tskrefs))::produtosComtasks(Nil)
  					case x::xs=>ProdutoComTasks(x.id,x.name,splitprodutoNum2(x.name),refstaskTotask(x.tskrefs))::produtosComtasks(xs)
  			
  			}
  
  		produtosComtasks(ptd)
  } 
    
  /**
   * Auxilia a função "productTodoTOproduto" (anterior)
   * 
   * @param Lista com referencias a tasks 
   * @return devolve Lista de tasks
   */
    def refstaskTotask(taskrefs:List[String]):List[Task]= taskrefs match{
 
     			case Nil=>Nil
     			case x::Nil => val task=mytasks.find ({ t => t.id==x })getOrElse(null)
     						if(task!=null) task::refstaskTotask(Nil) else refstaskTotask(Nil)
     					
     			case x::xs=> val task=mytasks.find ({ t => t.id==x })getOrElse(null)
     						if(task!=null) task::refstaskTotask(xs) else refstaskTotask(xs)
 	  }

    
   val produtoComTasks=productTodoTOproduto(myproducts)  //Lista de todos os produtos com objectos Task (e não refs de tasks)
   
//---agora quero criar uma lista de produtos a produzir com base na Ordem (OrdemProduto)- com numeração do produto correcta------    

   /**
    * Criar uma estrutura Ordem-Produto e numerar o produto
    * 
    * @param uma ordem
    * @return lista de "OrdemProduto"
    */
   def ordemToProdutos(o:Order):List[OrdemProduto]={

  		def numprodutos(quant:Int,nprod:Int):List[OrdemProduto]= quant match{
  				
  				case 0 => Nil
  				case _ => val prodd=prodrefToproduto(o.prdref)
  						if(prodd!=null){
  						
  						val numberprod=prodd.copy( nump=nprod)  //altera a propriedade nump
  							
  						 OrdemProduto(o.id,numberprod)::numprodutos(quant-1,nprod+1)
  						} else{ Nil }
				
		
          		}
          	
        numprodutos(o.quantity.toInt,1)

  }  
   
   /**
    * Auxilia a função "ordemToProdutos" (anterior). A partir da lista "produtoComTasks"
    * 
    * @param a lista "produtoComTasks"
    * @return cada um dos "ProdutoComTasks" identificados
    */
  def prodrefToproduto(prodid:String):ProdutoComTasks={
  
  		produtoComTasks.find ({ x => x.id==prodid }).getOrElse(null)
  
  }  
   
   //exemplo   val ordemProdutos= ordemToProdutos(Order("ORD_1","PRD_1",2))
  
   //da lista anterior só quero os produtos que podem ser produzidos (talvez só para a iteração 3)
 //collect em vez de map para evitar o erro (MatchError) - ( val ordemprodutosreduzida=ordemProdutos.collect...)
 //val ordemprodutosreduzida=ordemProdutos.collect { case x if (x.possoSerProduzida(myphysical, myhumans))=>x }
//----------------------------------------------------------------------------------------------------------------------   
 
  //trasformar ordem-Produto em ordem-produto-task dos produtos que podem ser produzidos (ordemprodutosreduzida)---------------
  /**
   * Transforma uma lista de "OrdemProduto" (ordem com produto com uma lista de tasks)
   * numa lista de "OrdemProdutoTask" (por cada task no produto transforma em ordem -> produto -> task)
   * 
   * @param uma lista de "OrdemProduto"
   * @return uma lista de "OrdemProdutoTask"
   */
  def OrderProductTask(ordemprods:List[OrdemProduto]):List[OrdemProdutoTask]= {
  
  			def orderprodstasks(ordemprodss:List[OrdemProduto]):List[OrdemProdutoTask]=ordemprodss match{
  			
  			case Nil=>Nil
  			case t::Nil => produtofromOrdemProduto(t):::orderprodstasks(Nil)
  			case t::ts => produtofromOrdemProduto(t):::orderprodstasks(ts)
  		}
  
  		orderprodstasks(ordemprods)
  
  } 
  
  /**
   * Auxilia a função "OrderProductTask" (anterior). "OrdemProduto" ordem com produto com uma lista de tasks
   * Por cada task na lista do produto devolve uma "OrdemProdutoTask"
   * 
   * @param uma "OrdemProduto"
   * @return uma lista de "OrdemProdutoTask"
   */
  def produtofromOrdemProduto(orderprod:OrdemProduto):List[OrdemProdutoTask]= {
		
    	def taskfromproduto(tasks:List[Task]):List[OrdemProdutoTask]= tasks match{
    		case Nil=>Nil
    		case t::Nil => OrdemProdutoTask(orderprod.ordId,
    																		ProdutoTask(orderprod.prod.id,orderprod.prod.name,orderprod.prod.nump,t))::taskfromproduto(Nil)
    		case t::ts => OrdemProdutoTask(orderprod.ordId,
    																		ProdutoTask(orderprod.prod.id,orderprod.prod.name,orderprod.prod.nump,t))::taskfromproduto(ts)
    			
    		}
		
			taskfromproduto(orderprod.prod.tasks)
		} 

  //exemplo 
  //val OrderProductTaskTeste=OrderProductTask(ordemprodutosreduzida)
 /* 
  def ordemProdTaskRed(ord:Order):List[OrdemProdutoTask]={
    
    val ordemproduto=ordemToProdutos(ord)  //estrutura ordem -> produto com lista de tasks
    
     //só os produtos que podem ser produzidos permanacem na estrutura
    val ordemprodutoRed=ordemproduto.collect { case x if (x.prod.possoSerProduzido(osRecursos.phys, osRecursos.hums))=>x }
    
    //não quero isto
    OrderProductTask(ordemprodutoRed) //tranforma a estrutura anterior na estrutura ordem -> produto -> task
    
  }
 */ 
  
    def ordemProdTaskRed2(ord:Order):List[OrdemProduto]={
    
    val ordemproduto=ordemToProdutos(ord)  //estrutura ordem -> produto com lista de tasks
    
     //só os produtos que podem ser produzidos permanacem na estrutura (se as tasks poderem ser produzidas)
    val ordemprodutoRed=ordemproduto.collect { case x if (x.prod.possoSerProduzido(osRecursos.phys, osRecursos.hums))=>x }
    
    //println("Nº Ordem produtos - "+ordemprodutoRed.toString())  //está certo
    ordemprodutoRed
    
    
  }
//-------------------------------------------------------------------------------------------------------------  
// funções necessarias em varias situações--------------------------------------------------------
   
   /**
    * Usa o "name="Product 2"" para retirar o numero para atribuir ao "nump" em ProdutoComTasks
    * Este numero é depois substituido pela numeração correcta 
    * 
    *  @param nome so produto
    *  @return devolve um inteiro (retira do nome)
    */
  def splitprodutoNum2(s:String):Integer={
 
 		val sssplit=s.indexOf(" ")
 		val num=s.slice(sssplit+1, s.length()).trim()
 	
 	try {
        //Some(num.toInt)
        num.toInt
    }catch{
        case e: NumberFormatException => 0
    }
    
 			
 }  
  
//----------------------------------------------------------------------------------------------------------------------  
    
}