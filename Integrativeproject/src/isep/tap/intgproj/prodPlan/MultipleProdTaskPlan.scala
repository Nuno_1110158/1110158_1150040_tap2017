package isep.tap.intgproj.prodPlan


import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._


case class MultipleProdTaskPlan(producao:Production) {
    
    val ordemProdutoPlan= OrdemProdutoPlanProduction(producao)
  
    
    val ordertaskPlan= OrderTaskPlanPlan()
    
    val recursos= ordemProdutoPlan.osRecursos
    
    /**
     * Recebe uma lista de ordens e por cada ordem transforma-a na estrutura "ordemproduto" (estrutura ordem -> produto com lista de tasks)
     * Filtra os produtos que podem ser produzidos (se uma ou varias tasks não poderem ser executadas o produto não pode ser produzido)
     *
     * cria em cada transformação uma lista das tasks que podem ser executadas e cria uma lista de "TasksSchedule"
     * 
     * @param lista de ordens
     * @return lista de "TasksSchedule"
     */
    def ordemsToTaskSchedule(ords:List[Order]):List[TasksSchedule]={
         
       def orderToTaskSchedule(ordss:List[Order],start:Int,end:Int):List[TasksSchedule]= ordss match{
            
          case Nil=>Nil
          case ord::Nil=>
                        //só os produtos que podem ser produzidos permanacem na estrutura
                       //tranforma a estrutura anterior na estrutura ordem -> produto -> task
                        val order_Product=ordemProdutoPlan.ordemProdTaskRed2(ord) 
                        
                        val instantezero=ordertaskPlan.instanteZero(order_Product, recursos, start, end)
                        
                        //tratar erro - UnsupportedOperationException: empty.maxBy (pode aparecer no meio uma lista vazia)
                        if(instantezero.length>0){
                           val endMax=instantezero.maxBy ({ tks => tks.end }).end  //maximo para o start
                        
                          instantezero:::orderToTaskSchedule(Nil,endMax,end)
                        }else{
                          instantezero:::orderToTaskSchedule(Nil,start,end)
                        }
                       
             
          case ord::ords=>
                        //val ordemproduto=ordemProdutoPlan.ordemToProdutos(ord)  //estrutura ordem -> produto
                        //só os produtos que podem ser produzidos permanacem na estrutura
            
                         val orderProductTask=ordemProdutoPlan.ordemProdTaskRed2(ord)
                         
                        val instantezero=ordertaskPlan.instanteZero(orderProductTask, recursos, start, end)

                          //tratar erro - UnsupportedOperationException: empty.maxBy (pode aparecer no meio uma lista vazia)
                        if(instantezero.length>0){
                           val endMax=instantezero.maxBy ({ tks => tks.end }).end  //maximo para o start
                        
                          instantezero:::orderToTaskSchedule(ords,endMax,end)
                        }else{
                          instantezero:::orderToTaskSchedule(ords,start,end)
                        }
                        
                        
        }
      
        println(ords.toString())
        orderToTaskSchedule(ords,0,0)
    }
    
    
    
}