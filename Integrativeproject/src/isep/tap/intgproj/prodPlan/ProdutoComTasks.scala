package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.model._

case class ProdutoComTasks(id:String,name:String,nump:Integer,tasks:List[Task]) {
  
  		// Só se todas as Tasks poderem ser executadas é que o produto pode ser feito
  
		def possoSerProduzido(phys:List[PhysicalResource], hums:List[HumanResource]):Boolean={
			
			tasks.forall { x => x.possoSerProduzida(phys, hums) }
			
		}
  
}

/**
 * Ordem com produto com a lista de tasks
 */
case class OrdemProduto(ordId:String,prod:ProdutoComTasks){

		//esta ordemProduto só pode ser produzida se o produto poder ser produzido
  
		def possoSerProduzida(phys:List[PhysicalResource], hums:List[HumanResource]):Boolean={
		
				prod.possoSerProduzido(phys, hums)
		}
		

}

/**
 * Estrutura ordem produto task - a mesma ordem pode ter mais do que um produto
 * e um produto pode ter mais do que uma task
 * por cada produto -> task existe uma "OrdemProdutoTask"
 * 
 */
//
case class OrdemProdutoTask(ordId:String,prodtask:ProdutoTask)

/**
 * produto com uma task - um produto pode ter mais do que uma task
 */
//
case class ProdutoTask(id:String,name:String,nump:Integer,task:Task)

/**
 * Pacote de objectos entre funções
 * 
 */
case class ObjectosEscalonamento(listTaskScheIntermedia:List[TasksSchedule],listTaskScheParaExec:List[TasksSchedule],
                                  listTaskScheResultado:List[TasksSchedule],lastEnd:Int,recs:Recursos)
 /**
  * Props: opList:List[OrdemProduto],opListParalelo:List[OrdemProduto],recs:Recursos                                
  */
case class OrdemProdutoRecursos(opList:List[OrdemProduto],opListParalelo:List[OrdemProduto],recs:Recursos)                                   

/**
 * Pacote de objectos entre funções
 * 
 */
case class ObjectosEscalonamentoV2(opList:List[OrdemProduto],listTaskScheIntermedia:List[TasksSchedule],listTaskScheParaExec:List[TasksSchedule],
                                 listLimbo:List[TasksSchedule],listTaskScheResultado:List[TasksSchedule],recs:Recursos)
                                  
                                  
                                  
                                  