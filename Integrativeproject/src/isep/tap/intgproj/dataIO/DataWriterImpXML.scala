package isep.tap.intgproj.dataIO

import scala.xml.XML
import scala.xml.PrettyPrinter
import java.io._
import isep.tap.intgproj.prodPlan.Schedule


class DataWriterImpXML extends DataWriter{
  
   override def writeIt(fname: String,schedule:Schedule):Option[String]={
     
         val whatFile:String = System.getProperty("user.dir")+"/files/"+fname
        
         val printer = new scala.xml.PrettyPrinter(80, 4)
         
        //devolve uma String formatada XML
        val prettyschedule=printer.format(scheduleNodeToXML(schedule))
        
         try {
                //Faz o carregamento para XML da String (anterior) e guarda num ficheiro
                XML.save(whatFile, XML.loadString(prettyschedule),"UTF-8",true)
         
               Some("Ficheiro Guardado")
         } catch {
           case e: Exception => None
         }

   }
   
     //devolve a composição do XML completo
     def scheduleNodeToXML(schedule:Schedule):xml.Node = schedule.noTaskScheduleList match{
 
       case true =>{schedule.noDataToXML}
       case false=>{schedule.toXML}
		      			
     } 
     
   //se não houver dados para imprimir  
   def noDatawriteIt(fname: String,schedule:Schedule):Option[String]= schedule.noTaskScheduleList match{
     
     case true =>{
       
         val whatFile:String = System.getProperty("user.dir")+"/files/"+fname
        
         val printer = new scala.xml.PrettyPrinter(80, 4)
         
        //devolve uma String formatada XML
        val prettyschedule=printer.format(scheduleNodeToXML(schedule))
        
         try {
                //Faz o carregamento para XML da String (anterior) e guarda num ficheiro
                XML.save(whatFile, XML.loadString(prettyschedule),"UTF-8",true)
         
               Some("Ficheiro Guardado")
         } catch {
           case e: Exception => None
         }
       
     }
     
     case false => None
         
   } 
   
    
  
}

