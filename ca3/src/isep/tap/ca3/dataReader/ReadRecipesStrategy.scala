package isep.tap.ca3.dataReader

object ReadRecipesStrategy {
  
   def apply(filename: String): Option[(String) => RecipeList] =
     filename match {
     case f if f.endsWith(".json") => Some(parseJson)
     case f if f.endsWith(".csv") => Some(parseCsv)
     case f => None
 }
   
   
private def parseJson(file: String): RecipeList = {
   val fromJSON=new DataReaderImplJSON
   
    fromJSON.read(file)
   
 }
 
 
private def parseCsv(file: String): RecipeList ={
   val fromCSV=new DataReaderImplCSV   
   
    fromCSV.read(file)
      
 }
 
 
}