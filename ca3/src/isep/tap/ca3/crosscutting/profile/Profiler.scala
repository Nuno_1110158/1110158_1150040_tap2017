package isep.tap.ca3.crosscutting.profile

import isep.tap.ca3.dataReader.DataReader
import isep.tap.ca3.dataReader.RecipeList

/*
 * DONE: create the Profiler trait, which forces the reader to return a ProfilerList
 */

trait Profiler extends DataReader{  

  abstract override def readDataFast(fname: String): ProfilerList[Long] = {
    val t0 = System.nanoTime()
    val result = super.readDataFast(fname)
    val t1 = System.nanoTime()
    new ProfilerList(result.lr,t1-t0)
  }
    

  abstract override def readDataSlow(fname: String): ProfilerList[Long] = {
    val t0 = System.nanoTime()
    val result = super.readDataSlow(fname)
    val t1 = System.nanoTime()
    new ProfilerList(result.lr,t1-t0)
    
  }
}