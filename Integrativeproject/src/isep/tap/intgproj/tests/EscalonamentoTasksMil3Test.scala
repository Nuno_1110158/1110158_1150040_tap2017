package isep.tap.intgproj.tests

import scala.xml.XML
import org.scalatest.FunSuite
import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._
import isep.tap.intgproj.prodPlan._

class EscalonamentoTasksMil3Test extends FunSuite{
  
  val fxml="ip_2017_in_ok_2o_3p_2t_v1.xml"  
  val falsofxml="aaaa.xml"
  
  val iniciarPlanoescalonamento=EscalonamentoTasksMil3(fxml)
  val objectsPlanTeste=ObjectsPlanMil3(iniciarPlanoescalonamento.produzir)
  val definirEscalonamento= EscalonamentoTasksPlanMil3()

   val ordProdutosQuePodemSerProduzidos=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens) 
     
   val objectoParaEscalonar=ObjectosEscalonamentoV2(ordProdutosQuePodemSerProduzidos,Nil,Nil,Nil,Nil,iniciarPlanoescalonamento.osRecursos)	
       
   val tasksPodemSerExecParalelo=definirEscalonamento.tasksPodemSerExec(objectoParaEscalonar.opList, objectoParaEscalonar.recs)
       
    test("Teste 1 verificar a existencia de dados...") {

     val iniciarPlanoescalonamentoSemDados=EscalonamentoTasksMil3(falsofxml)
     
     val exitemDados=iniciarPlanoescalonamento.existemDadosFromFile
     val naoExitemDados=iniciarPlanoescalonamentoSemDados.existemDadosFromFile
     
      assert(exitemDados===true)
      assert(naoExitemDados===false)
      
    }
  
  
    test("Teste 2 verificar OrdemProduto que podem ser produzidos...") {  
        //apenas para o ficheiro "ip_2017_in_ok_2o_3p_2t_v1.xml" 
        val listaOrdemProdutoPodemSerProduzidos=List(OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",1,List(Task("TSK_1",10,List("PRST 1")), Task("TSK_2",15,List("PRST 2"))))), 
                                  OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",2,List(Task("TSK_1",10,List("PRST 1")), Task("TSK_2",15,List("PRST 2"))))), 
                                  OrdemProduto("ORD_2",ProdutoComTasks("PRD_2","Product 2",1,List(Task("TSK_2",15,List("PRST 2"))))), 
                                  OrdemProduto("ORD_2",ProdutoComTasks("PRD_2","Product 2",2,List(Task("TSK_2",15,List("PRST 2"))))))
      
        val listaOrdersProdsTasksExecutar=objectsPlanTeste.ordersProdsTasksExecutar(iniciarPlanoescalonamento.asOrdens)                          
                                  
        assert(listaOrdersProdsTasksExecutar===listaOrdemProdutoPodemSerProduzidos)   
        
    }
  
  test("Teste 3 tasks que podem ser Executadas em paralelo...") {  
    
      //primeira iteração - produtos que podem ser produzidos em paralelo
      val tasksPodemSerExecParaleloTeste=List(OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",1,List(Task("TSK_1",10,List("PRST 1")), Task("TSK_2",15,List("PRST 2"))))), 
                OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",2,List(Task("TSK_1",10,List("PRST 1")), Task("TSK_2",15,List("PRST 2"))))), 
                OrdemProduto("ORD_2",ProdutoComTasks("PRD_2","Product 2",1,List(Task("TSK_2",15,List("PRST 2"))))))
      
       
        assert(tasksPodemSerExecParalelo===tasksPodemSerExecParaleloTeste)  
              
  }
  
  test("Teste 4 Das tasks executadas em paralelo da-me as tasksSchedule...") { 
    
        //da primeira iteração - da-me as tasksSchedules
        val dameTasksScheduleTeste=List(TasksSchedule("ORD_1",1,"TSK_1",0,10,List(PhysicalResource("PRS_1","PRST 1")),List(HumanResource("HRS_1","Antonio",List("PRST 1")))), 
              TasksSchedule("ORD_1",2,"TSK_1",0,10,List(PhysicalResource("PRS_2","PRST 1")),List(HumanResource("HRS_2","Maria",List("PRST 1", "PRST 2")))), 
              TasksSchedule("ORD_2",1,"TSK_2",0,15,List(PhysicalResource("PRS_3","PRST 2")),List(HumanResource("HRS_3","Jose",List("PRST 2")))))
  
        
        val dameTasksSchedule=definirEscalonamento.dameTasksSchedules(tasksPodemSerExecParalelo, 0, 0, iniciarPlanoescalonamento.osRecursos)  
        
        assert(dameTasksSchedule===dameTasksScheduleTeste) 
      
  }
  
   test("Teste 5 Remover task já produzidas da lista do OrdemProduto (por produto)...") {
     
             //primeira iteração - produtos que podem ser produzidos em paralelo
        val tasksPodemSerExecParaleloTeste=List(OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",1,List(Task("TSK_1",10,List("PRST 1")), Task("TSK_2",15,List("PRST 2"))))), 
                  OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",2,List(Task("TSK_1",10,List("PRST 1")), Task("TSK_2",15,List("PRST 2"))))), 
                  OrdemProduto("ORD_2",ProdutoComTasks("PRD_2","Product 2",1,List(Task("TSK_2",15,List("PRST 2"))))))
                  
        val comTaskRemovidaTeste=List(OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",1,List(Task("TSK_2",15,List("PRST 2"))))), 
                  OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",2,List(Task("TSK_2",15,List("PRST 2"))))), 
                  OrdemProduto("ORD_2",ProdutoComTasks("PRD_2","Product 2",1,List()))) 
                  
        val semProdutoComListaTasksVazia=List(OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",1,List(Task("TSK_2",15,List("PRST 2"))))), 
                  OrdemProduto("ORD_1",ProdutoComTasks("PRD_1","Product 1",2,List(Task("TSK_2",15,List("PRST 2")))))) 
                            
        //remover task do produto                  
        assert(definirEscalonamento.removerTaskDoProduto(tasksPodemSerExecParaleloTeste)===comTaskRemovidaTeste) 
        
        //remover produto com lista de task vazia
        assert(definirEscalonamento.removerDaListaProdsSemTasks(comTaskRemovidaTeste)===semProdutoComListaTasksVazia) 
        
   }
  
  test("Teste 6 ordenar taskSchedule...") {
    
          val taskSchedule=List(TasksSchedule("ORD_1",1,"TSK_1",15,30,List(PhysicalResource("PRS_1","PRST 1")),
											List(HumanResource("HRS_1","Antonio",List("PRST 1")))),
											TasksSchedule("ORD_1",1,"TSK_1",0,15,List(PhysicalResource("PRS_1","PRST 1")),
											List(HumanResource("HRS_1","Cristina",List("PRST 1")))),
											TasksSchedule("ORD_1",1,"TSK_1",0,10,List(PhysicalResource("PRS_1","PRST 1")),
											List(HumanResource("HRS_1","Maria",List("PRST 1")))))
											
					val taskScheduleOrdenadaTeste=List(TasksSchedule("ORD_1",1,"TSK_1",0,10,List(PhysicalResource("PRS_1","PRST 1")),
											List(HumanResource("HRS_1","Maria",List("PRST 1")))),
	                    TasksSchedule("ORD_1",1,"TSK_1",0,15,List(PhysicalResource("PRS_1","PRST 1")),
											List(HumanResource("HRS_1","Cristina",List("PRST 1")))),	
											TasksSchedule("ORD_1",1,"TSK_1",15,30,List(PhysicalResource("PRS_1","PRST 1")),
											List(HumanResource("HRS_1","Antonio",List("PRST 1")))))
											
			  //ordenar taskSchedules - start e end
        assert(definirEscalonamento.taskScheduleOrdenadaZA(taskSchedule)===taskScheduleOrdenadaTeste) 						
  }
  
  
}