
import scala.xml.XML
import scala.xml.PrettyPrinter
import isep.tap.intgproj.model._
import java.io._
import isep.tap.intgproj.prodPlan._
import isep.tap.intgproj.dataIO.DataWriterImpXML


//import isep.tap.intgproj.prodPlan._

object worksheet22 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(293); 
  println("Welcome to the Scala worksheet");$skip(1365); 
 
 //----------------------------------------------------------------------------------------------
 /*
 case class TaskSchedule(order:String,productNum:Integer,task:String,start:Integer,end:Integer,
                        physResources:List[PhysicalResource],humanResources:List[HumanResource]) {
  
  def toXML:xml.Node = {
    
	<TaskSchedule order={order} productNumber={productNum.toString()} task={task} start={start.toString()} end={end.toString()}>
		<PhysicalResources>
			{physResources.map(phyresourceToXML)}
		</PhysicalResources>
		<HumanResources>
			{humanResources.map(humanresourcesToXML)}
		</HumanResources>
	</TaskSchedule>

  }
    
   def phyresourceToXML(phyresource:PhysicalResource):xml.Node ={
   
   			phyresource.toXML
   }
    
  
  def humanresourcesToXML(humanresource:HumanResource):xml.Node ={
  
  			humanresource.toXML
  
		}
		
}
 
 case class Schedule(taskScheduleList:List[TaskSchedule]){
 
 			def toXML:xml.Node={
<Schedule xmlns="http://www.dei.isep.ipp.pt/ip_2017" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/ip_2017 ip_2017_out.xsd ">
	{taskScheduleList.map(tkS=>tkS.toXML)}
</Schedule>
 			
 			}
 
 }
 
*/
 // ---------------------------------------------------------------------------------------------
  val physResources1=PhysicalResource("PRS_1","PRST 1");System.out.println("""physResources1  : isep.tap.intgproj.model.PhysicalResource = """ + $show(physResources1 ));$skip(56); 
  val physResources2=PhysicalResource("PRS_2","PRST 2");System.out.println("""physResources2  : isep.tap.intgproj.model.PhysicalResource = """ + $show(physResources2 ));$skip(85); 
  
  
   val humanResource1=HumanResource("HRS_1","Antonio",List("PRST 1","PRST 2"));System.out.println("""humanResource1  : isep.tap.intgproj.model.HumanResource = """ + $show(humanResource1 ));$skip(81); 
   
   val humanResource2=HumanResource("HRS_2","Maria",List("PRST 1","PRST 2"));System.out.println("""humanResource2  : isep.tap.intgproj.model.HumanResource = """ + $show(humanResource2 ));$skip(233); 
                                                  
                                                  
   val taskschedule=TasksSchedule("ORD_1",1,"TSK_1",0,100,List(physResources1,physResources2),List(humanResource1,humanResource2));System.out.println("""taskschedule  : isep.tap.intgproj.prodPlan.TasksSchedule = """ + $show(taskschedule ));$skip(49); 
   val  theSchedule=Schedule(List(taskschedule));System.out.println("""theSchedule  : isep.tap.intgproj.prodPlan.Schedule = """ + $show(theSchedule ));$skip(172); 
 //------------------------------------------------------------------------------------------------------------------------------
 
 val datawriterxml=new DataWriterImpXML;System.out.println("""datawriterxml  : isep.tap.intgproj.dataIO.DataWriterImpXML = """ + $show(datawriterxml ));$skip(65); 
 
 
 	datawriterxml.writeIt("teste_out1_2017_.xml", theSchedule);$skip(70); 
 	
  datawriterxml.writeItPretty("teste_out1_2017_.xml", theSchedule)}
  
 
 //-----------------------------------------------------------------------------------------------------------------------------
 /*
 	val printer = new scala.xml.PrettyPrinter(80, 4)
 
  
  def scheduleNodeToXML:xml.Node ={
 
		{theSchedule.toXML}

  			
  }
  
	
	val fileN="D:/ISEP/ISEP_Mestrado/Semestre_2/TAP/CA3/Integrativeproject/files/teste_out1_2017_.xml"
	
  val fileN2="D:/ISEP/ISEP_Mestrado/Semestre_2/TAP/CA3/Integrativeproject/files/output.xml"
	
	val tttt=printer.format(scheduleNodeToXML)
	
      val writer = new PrintWriter(new File(fileN2))
      writer.write(tttt)
      writer.close()
	


	
	XML.save(fileN, scheduleNodeToXML,"UTF-8",true)
	//	XML.save(filename, node, enc, xmlDecl, doctype)
	

  */
  
}
