package isep.tap.intgproj.prodPlan


import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._


case class DoTaskScheduleByOrder(fname:String) {
  
    val dataReadImpXML= new DataReaderImpXML
    
    val escreverTasksSchedule=new DataWriterImpXML
    
    val ordemProduction=dataReadImpXML.read(fname)
    
    
    val multipleProdTaskPlan=MultipleProdTaskPlan(produzir)  //instânciar classe, não consigo com None
    
    
    def criarTasksScheduleToFile(fnameOut:String):Option[String]={
      
      if(existemDados){ 
       val tasksschedule= multipleProdTaskPlan.ordemsToTaskSchedule(asOrdens)
        
        escreverTasksSchedule.writeIt(fnameOut, Schedule(tasksschedule))
        
      }else{
        
        escreverTasksSchedule.writeIt(fnameOut, Schedule(Nil))
      }
            
    }
    
    
    def produzir:Production={
      
        if(existemDados) ordemProduction.get
        else Production(Nil,Nil,Nil,Nil,Nil)
    }
    
    //entrada MultipleProdTaskPlan()
    
    
    /**
     * Verifica se existem dados de devolve true ou false
     * 
     * @return devolve true ou false
     */
    def existemDados:Boolean={
      
        if(ordemProduction==None) false else true
    }
    
     /**
     * Devolve as ordens ou lista vazia
     * 
     * @return devolve lista de ordens
     */
    def asOrdens:List[Order]={
      
      if(existemDados) ordemProduction.get.orders else Nil
      
    }
    
    /**
     * Devolve os produtos ou lista vazia
     * 
     * @return devolve lista de produtos
     */
    def osProdutos:List[ProductToDo]={
      
        if(existemDados) ordemProduction.get.products else Nil
    }
    
    /**
     * Devolve as tasks ou lista vazia
     * 
     * @return devolve lista de tasks
     */
    def asTasks:List[Task]={
      
      if(existemDados) ordemProduction.get.tasks else Nil
    }
    
    
    /**
     * Devolve os recursos fisicos e humanos ou lista vazia
     * 
     * @return devolve lista de recursos fisicos e humanos
     */
    def osRecursos:Recursos={
      
      if(existemDados) Recursos(ordemProduction.get.fResources,ordemProduction.get.hResources) 
      else Recursos(Nil,Nil)      
        
    }
  
}