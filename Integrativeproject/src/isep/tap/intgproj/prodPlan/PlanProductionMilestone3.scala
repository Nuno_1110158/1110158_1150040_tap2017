package isep.tap.intgproj.prodPlan

import scala.xml
import isep.tap.intgproj.model._


case class PlanProductionMilestone3 (physicalResources:List[PhysicalResource],
                      taskResources:List[Task],
                      humanResources:List[HumanResource],
                      productResources:List[ProductToDo]) {
  
  /*
   * Converte uma lista de objectos Order input numa lista de objectos TasksSchedule de output
   * @param l a lista de ordens
   * @param timer O tempo que já foi despendido até à ordem actual.
   */
  def orderIntoSchedule(l : List[Order], OGTasks: List[String], currentTasks: List[String], OGQuantity: Int, quantity: Int, productNumber: Int, timer: Int, prl: List[PhysicalResource], hl: List[HumanResource], tasksDone: List[TasksSchedule], starting: Boolean) : List[TasksSchedule] = {
    //Se estivermos numa nova ordem inicializamos algumas variáveis.
    if(starting){
      val tasks = productResources.find(p => p.id == l.head.prdref).get.tskrefs
      return orderIntoSchedule(l,tasks,tasks, l.head.quantity, l.head.quantity, 1, timer, prl, hl, tasksDone, false);
    }
    
    
    if(l.length > 0) {
      //Tenta realizar ordem
      val tasks = ProcessPrimaryProduct(l.head, currentTasks, currentTasks,timer, l.head.quantity, l.head.quantity, 1, Nil, Nil, tasksDone)
      val physR = tasks.map(t => t.physResources);
      if(tasks.length != 0 && l.length > 1){
        //Tenta realizar outras ordens em paralelo
        val parallelOrderTasks = tasks:::TryDoingOrderInParallel(l.tail,timer, prl:::tasks.map(t => t.physResources).flatten, hl:::tasks.map(t => t.humanResources).flatten,tasksDone:::tasks)
        val parallelTasksEnd = parallelOrderTasks.max(Ordering.by(((t:TasksSchedule) => t.end))).end;
        if(currentTasks.length > 1) {
          //Se houver mais tasks passa para a seguinte
          return parallelOrderTasks:::orderIntoSchedule(l,OGTasks,currentTasks.tail,OGQuantity,quantity,productNumber,parallelTasksEnd, Nil,Nil,tasksDone:::parallelOrderTasks,false);
        } else if(quantity > 1) {
          //Se houver mais quantidade passa para o próximo produto
          return parallelOrderTasks:::orderIntoSchedule(l,OGTasks,OGTasks,OGQuantity,quantity-1,productNumber+1,parallelTasksEnd, Nil,Nil,tasksDone:::parallelOrderTasks,false);
        } else if(l.length > 1) {
          //Se houver mais uma ordem passa para ela
          return parallelOrderTasks:::orderIntoSchedule(l.tail,Nil,Nil,0,0,0,parallelTasksEnd, Nil,Nil,tasksDone:::parallelOrderTasks,true);
        } else {
          parallelOrderTasks
        }
      } else if(quantity > 1) {
        return orderIntoSchedule(l,OGTasks,OGTasks,OGQuantity,quantity-1,productNumber+1,timer, Nil,Nil,tasksDone,false);
      } else if(l.length > 1) {
        return orderIntoSchedule(l.tail,Nil,Nil,0,0,0,timer, Nil,Nil,tasksDone,true);
      } else {
        tasks
      }
    } else {
      Nil
    }
  }
    /*
     * Tenta realizar uma ordem em paralelo
     */
    def TryDoingOrderInParallel(l : List[Order], timer: Int, prl: List[PhysicalResource], hl: List[HumanResource], tasksDone: List[TasksSchedule]) : List[TasksSchedule] = {
      //Passa para a ordem seguinte
      val tasks = ProcessPrimaryProduct(l.head, productResources.find(p => p.id == l.head.prdref).get.tskrefs, productResources.find(p => p.id == l.head.prdref).get.tskrefs,timer, l.head.quantity, l.head.quantity, 1, prl, hl, tasksDone);
      return tasks
    }
  
  /*
   * Processa o produto primário, começando pela primeira task do primeiro produto
   * @param o A ordem em questão
   * @param OGTasks A lista de Tasks do produto
   * @param p A lista de tasks por realizar
   * @param timer O tempo despendido atÃ© ao momento
   * @param OGQuantity O número de produtos ainda por finalizar
   * @param quantity O número de produtos ainda por finalizar
   * @param productNumber O productNumber do produto actual
   * @param prl Recursos físicos ocupados neste momento
   * @param hl Recursos humanos ocupados neste momento
   * @param tasksDone as tasks já realizadas
   */
  def ProcessPrimaryProduct(o: Order, OGTasks: List[String],  p: List[String], timer: Int, OGQuantity: Int, quantity: Int, productNumber: Int, prl: List[PhysicalResource], hl: List[HumanResource], tasksDone: List[TasksSchedule]) : List[TasksSchedule] = {
    if(quantity > 0){
      if(tasksDone.find(t => t.productNum == productNumber && t.task == p.head && t.order == o.id) != None){
        if(p.length > 1){
          //Se ainda hÃ¡ produtos por fazer, se a task do produto actual já¡ foi feita e se ainda há¡ tasks
          //por fazer, entÃ£o passa para a task seguinte
          return ProcessPrimaryProduct(o,OGTasks,p.tail,timer,OGQuantity,quantity,productNumber,prl,hl,tasksDone);
        } else {
          //senÃ£o significa que o produto actual está terminado, então passamos para o produto seguinte.
          return ProcessPrimaryProduct(o,OGTasks,OGTasks,timer,OGQuantity-1,OGQuantity-1,productNumber+1,prl,hl,tasksDone);
        }
      }
      //Realiza a task actual do produto primário
      val task = getProducts(o, p, timer, productNumber, prl, hl)
      //Se não for retornado nenhum TasksScheduled é porque o produto contém uma task que não existe,
      //nesse caso passamos para o produto seguinte.
      //O correcto seria verificar no OrderIntoSchedule se existem tasks no produto que não existem na lista
      //tasksResources e passar para a ordem seguinte.
      if(task.length == 0){
        return ProcessPrimaryProduct(o,OGTasks,OGTasks,timer,OGQuantity-1,OGQuantity-1,productNumber+1,prl,hl, tasksDone);
      }
      //Tenta realizar outras tasks dos produtos seguintes em paralelo
      val parallelTasks = task:::TryDoingProductInParallel(o,OGTasks,OGTasks,timer,OGQuantity,quantity-1,productNumber+1,prl:::task.head.physResources,hl:::task.head.humanResources, task:::tasksDone);
      //Obtém a duração máxima das tasks realizadas em paralelo
      val parallelTasksEnd = parallelTasks.max(Ordering.by(((t:TasksSchedule) => t.end))).end;
      parallelTasks
      /*if(p.length > 1){
        //Se ainda houver tasks por fazer no produto actual, passa para a prÃ³xima task
        parallelTasks:::ProcessPrimaryProduct(o,OGTasks,p.tail,parallelTasksEnd,OGQuantity,quantity,productNumber,Nil,Nil, tasksDone:::parallelTasks);
      } else {
        //senÃ£o passa para o produto seguinte
        parallelTasks:::ProcessPrimaryProduct(o,OGTasks,OGTasks,parallelTasksEnd,OGQuantity-1,OGQuantity-1,productNumber+1,Nil,Nil, tasksDone:::parallelTasks);
      }*/
    } else {
      Nil
    }
  }
  
  /*
   * Tenta realizar tarefas em paralelo tendo em conta os recursos que se encontram actualmente alocados
   * @param o A ordem em questão
   * @param OGTasks A lista de Tasks do produto
   * @param p A lista de tasks por realizar
   * @param timer O tempo despendido atÃ© ao momento actual
   * @param OGQuantity O número de produtos ainda por finalizar
   * @param quantity O número de produtos ainda por finalizar
   * @param productNumber O productNumber do produto actual
   * @param prl Recursos físicos ocupados neste momento
   * @param hl Recursos humanos ocupados neste momento
   * @param tasksDone as tasks já realizadas
   */
  def TryDoingProductInParallel(o: Order, OGTasks: List[String],  p: List[String], timer: Int, OGQuantity: Int, quantity: Int, productNumber: Int, prl: List[PhysicalResource], hl: List[HumanResource], tasksDone: List[TasksSchedule]) : List[TasksSchedule] = {
    if(tasksDone.find(t => t.productNum == productNumber && t.task == p.head && t.order == o.id) != None){
      if(p.length > 1){
        //Se a task do produto actual jÃ¡ foi feita e se ainda hÃ¡ tasks por fazer, entÃ£o passa para a task seguinte
        val nextTask = TryDoingProductInParallel(o,OGTasks,p.tail,timer,OGQuantity,quantity, productNumber,prl,hl, tasksDone);
        return nextTask
      } else {
        //senÃ£o significa que o produto actual estÃ¡ terminado, entÃ£o passamos para o produto seguinte.
        val nextTask = TryDoingProductInParallel(o,OGTasks,OGTasks,timer,OGQuantity,quantity-1, productNumber+1,prl,hl, tasksDone);
        return nextTask
      }
    }
    if(quantity > 0) {
      //Realiza a task do produto actual
      val task = getProducts(o,p,timer,productNumber,prl,hl);
      if(task == Nil){
        //Se a task vier a vazio significa que não há condições para a realizar agora, então passamos para 
        //o produto seguinte
        val nextTask = TryDoingProductInParallel(o,OGTasks,OGTasks,timer,OGQuantity,quantity-1, productNumber+1,prl,hl, task:::tasksDone);
        nextTask
      } else {
        //senão realizamos a task e tentamos ver se ainda se pode fazer outra task em paralelo de um
        //produto seguinte com os recursos ainda disponíveis
        val nextTask = task:::TryDoingProductInParallel(o,OGTasks,OGTasks,timer,OGQuantity-1,quantity-1,productNumber+1,task.head.physResources:::prl,task.head.humanResources:::hl, task:::tasksDone);
        nextTask
      }    
    } else {
      Nil
    }
  }
   
  /*
   * Realiza uma task do produto actual. Apesar de retornar uma lista de TasksSchedule esta lista terá sempre
   * somente um elemento. Falta refatorizar para retornar uma Option[TasksSchedule] em vez do actual.
   * @param o A ordem em questÃ£o
   * @param p lista de tasks da qual só a primeira será¡ realizada (falta refatorizar para passar só uma task)
   * @param timer O tempo despendido atÃ© ao momento actual
   * @param productNumber O productNumber do produto actual
   * @param prl Recursos físicos ocupados neste momento
   * @param hl Recursos humanos ocupados neste momento
   */
  def getProducts(o: Order, p: List[String], timer: Int, productNumber: Int, prl: List[PhysicalResource], hl: List[HumanResource]) : List[TasksSchedule] = p match {
    case x::xs => {
      if(taskResources.find(t => t.id == x) == None) {
        Nil
      } else {
        val ts = getTasks(o, p, taskResources.find(t => t.id == x).get, taskResources.find(t => t.id == x).get.physicRtype,prl: List[PhysicalResource], hl: List[HumanResource],timer, productNumber)
        ts
        //Como só pretendemos obter uma task de cada vez o seguinte código da milestone1 fica comentado
        /*if(ts.isEmpty) {
          ts
        } else {
          ts:::getProducts(o,p.tail, ts.last.end, productNumber)
        }*/
      }
    }
    case Nil => Nil
  }
  
  /*
   * Auxilia o método getTasks obtendo os resources extras necessários no caso de uma task ter mais do que
   * um physicRType
   * task e cria o TasksScheduled.
   * @param o A ordem em questÃ£o
   * @param p lista de tasks
   * @param t A task a ser realizada neste momento
   * @param physicRType o physicRtype da task a ser realizada neste momento
   * @param prl Recursos físicos ocupados neste momento
   * @param hl Recursos humanos ocupados neste momento
   * @param timer O tempo despendido até ao momento actual
   * @param productNumber O productNumber do produto actual
   */
  def getTasksAux(o: Order, p: List[String], t: Task, physicRType: List[String], prl: List[PhysicalResource], hl: List[HumanResource]) : List[Resource] = physicRType match {
    case x::Nil => {
      //Se for o último physicRType obtém os recursos necessários e retorna-os
      if(physicalResources.find(p => p.typeR == x) == None) {
        Nil
      } else {
        val resource = getPhysicalResource(o, p, t, physicalResources.find(p => p.typeR == x).get,prl,hl)
        resource
      }
    }
    case x::xs => {
      //Se não for o último physicRType obtém os recursos necessários para o physicRType actual e passa
      //para o seguinte
      if(physicalResources.find(p => p.typeR == x) == None) {
        Nil
      } else {
        val resource = getPhysicalResource(o, p, t, physicalResources.find(p => p.typeR == x).get,prl,hl)
        if(resource == Nil || getTasksAux(o,p,t,physicRType.tail,prl:::resource.map(r => r.physicalResource),hl:::resource.map(r => r.operator)) == Nil) {
          return Nil
        }
        resource:::getTasksAux(o,p,t,physicRType.tail,prl:::resource.map(r => r.physicalResource),hl:::resource.map(r => r.operator))
      }
    }
    case Nil => Nil
  }
  
  /*
   * Para cada recurso físico utilizado por uma task obtém o recurso físico e o recursos humano usado pela
   * task e cria o TasksScheduled.
   * @param o A ordem em questÃ£o
   * @param p lista de tasks
   * @param t A task a ser realizada neste momento
   * @param physicRType Os physicRtypes da task a ser realizada neste momento
   * @param prl Recursos físicos ocupados neste momento
   * @param hl Recursos humanos ocupados neste momento
   * @param timer O tempo despendido até ao momento actual
   * @param productNumber O productNumber do produto actual
   */
  def getTasks(o: Order, p: List[String], t: Task, physicRType: List[String], prl: List[PhysicalResource], hl: List[HumanResource], timer: Int, productNumber: Int) : List[TasksSchedule] = physicRType match {
    case x::xs => {
      if(physicalResources.find(p => p.typeR == x) == None){
        Nil
      } else {
        //Obtém o recurso físico e humano necessário para a o physicRType actual
        val resource = getPhysicalResource(o, p, t, physicalResources.find(p => p.typeR == x).get,prl,hl)
        if(resource == Nil){
          return Nil;
        }
        if(physicRType.length > 1){
          //Se existirem mais physicRType nesta task mas não for possível obter recursos para os realizar,
          //então significa que a task nÃ£o pode ser realizada e retornamos Nil
          if(getTasksAux(o,p,t,physicRType.tail,prl:::resource.map(r => r.physicalResource),hl:::resource.map(r => r.operator)) == Nil){
            return Nil;
          }
          //Obtemos os recursos necessários para os physicRType seguintes
          val resourceNew = resource:::getTasksAux(o,p,t,physicRType.tail,prl:::resource.map(r => r.physicalResource),hl:::resource.map(r => r.operator).sortBy(h => (h.handeltype.size, h.id)));
          val ts = new TasksSchedule(o.id,productNumber,t.id,timer,timer+t.time,resourceNew.map(r => r.physicalResource),resourceNew.map(r => r.operator).sortBy(h => (h.handeltype.size, h.id)));
          if(resourceNew != Nil){
            //Criamos o TasksSchedule com os recursos que ele necessita
            val ts = new TasksSchedule(o.id,productNumber,t.id,timer,timer+t.time,resourceNew.map(r => r.physicalResource),resourceNew.map(r => r.operator).sortBy(h => (h.handeltype.size, h.id)));
            val tsList : List[TasksSchedule] = List(ts)
            tsList
          } else {
            Nil
          }
        } else {
          //Caso só haja um physicRType nesta task
          val resourceNew = resource;
          val ts = new TasksSchedule(o.id,productNumber,t.id,timer,timer+t.time,resourceNew.map(r => r.physicalResource),resourceNew.map(r => r.operator).sortBy(h => (h.handeltype.size, h.id)));
          if(resourceNew != Nil){
            val ts = new TasksSchedule(o.id,productNumber,t.id,timer,timer+t.time,resourceNew.map(r => r.physicalResource),resourceNew.map(r => r.operator).sortBy(h => (h.handeltype.size, h.id)));
            val tsList : List[TasksSchedule] = List(ts)
            tsList
          } else {
            Nil
          }
        }
      }
    }
    case Nil => Nil
  }
  
  /*
   * Obtém o recurso físico disponível para um dado tipo de recurso físico e o recurso humano capaz de operar
   * o recurso físico em causa.
   * @param o A ordem em questão
   * @param p A lista de tasks
   * @param t A task a ser realizada neste momento
   * @param pr O recurso físico necessário
   * @param prl Recursos físicos ocupados neste momento
   * @param hl Recursos humanos ocupados neste momento
   */
  def getPhysicalResource(o: Order, p: List[String], t: Task, pr: PhysicalResource, prl: List[PhysicalResource], hl: List[HumanResource]) : List[Resource] = {
    //Obtém a lista de recursos físicos livres
    val physicalResourcesAvailable = physicalResources.filter(p => !prl.contains(p));
    //Verifica se dos recursos físicos livres há algum do tipo que precisamos, se não houver retorna Nil
    if(physicalResourcesAvailable.find(p => p.typeR == pr.typeR) == None){
      Nil
    } else {
      val physicalResourceObj = physicalResourcesAvailable.find(p => p.typeR == pr.typeR).get;
      //Obtém a lista de recursos humanos livres
      val humanResourcesAvailable = humanResources.filter(h => !hl.contains(h)).sortBy(h => (h.handeltype.size, h.id));
      //Verifica se dos recursos humanos livres há algum capaz de operar o recurso físico em causa, se não houver retorna Nil
      if(humanResourcesAvailable.find(h => h.handeltype.indexOf(physicalResourceObj.typeR) != -1) == None) {
        //val resource = new Resource(physicalResourceObj, new HumanResource("","",List())) :: Nil;
        //resource
        Nil
      } else {
        val humanResource = humanResourcesAvailable.find(h => h.handeltype.indexOf(physicalResourceObj.typeR) != -1).get;
        //Cria e retorna os recursos necessários
        val resource = new Resource(physicalResourceObj,humanResource) :: Nil;
        resource
      }
    }
  }
    
}