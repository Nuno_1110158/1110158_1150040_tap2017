package isep.tap.ca3.crosscutting.profile

import isep.tap.ca3.model.Recipe
import isep.tap.ca3.dataReader.RecipeList

class ProfilerList[A](override val lr: List[Recipe], val pt: A) extends RecipeList(lr)