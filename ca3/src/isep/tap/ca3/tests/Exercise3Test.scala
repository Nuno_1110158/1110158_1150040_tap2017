package isep.tap.ca3.tests


import org.scalatest.FunSuite
import isep.tap.ca3.model.Recipe
import isep.tap.ca3.dataReader.DataReaderImplCSV
import isep.tap.ca3.crosscutting.profile.Profiler
import isep.tap.ca3.dataReader
import isep.tap.ca3.dataReader.DataReaderImplJSON
import isep.tap.ca3.dataReader.ReadRecipesStrategy
import isep.tap.ca3.dataReader.RecipeList


class Exercise3Test extends FunSuite {
  
  /* TODO: uncomment the test when DataReaderImplCSV is created */
  /*
   *   
  */
   
  test("Exercise 3 initial conditions...") {
    val dataReader = new DataReaderImplCSV  with Profiler
    
    val expected = List(Recipe("Portuguese Green Soup","Portugal"), Recipe("Grilled Sardines","Portugal"), Recipe("Salted Cod with Cream","Portugal"))
    val other_expected =List(Recipe("German Sauerkraut","Germany"),Recipe("Risotto","Italy"),Recipe("Tortilla","Spain"))

    assert(dataReader.readDataFast("recipe.csv").lr===expected)
    assert(dataReader.readDataSlow("other_recipe.csv").lr===other_expected)
  }
 
  
    
  /* TODO: create the tests */

  // strategy does not exist
  test("Exercise 3. Test 1. - Verify strategy - None") {

       assert(ReadRecipesStrategy("recipe.xls")===None)      
    
  }
  
    //teste da estrategia mais abaixo.
  
  // read one file JSON and another csv, with same content, verify equality
  test("Exercise 3. Test 2 - Verify equality.") {
     val dataReaderCSV = ReadRecipesStrategy("recipe.csv").getOrElse(x:String=>new RecipeList())
     val dataReaderJSON = new DataReaderImplJSON
     
     val expectedJSON = dataReaderJSON.readDataFast("recipe.json").lr
     val expectedCSV = dataReaderCSV("recipe.csv").lr
      
     assert(expectedJSON===expectedCSV)
    
  }

  // read one file JSON and another csv, with different content, verify inequality
  test("Exercise 3. Test 3 - Verify inequality.") {
     val dataReaderCSV = new DataReaderImplCSV
     val dataReaderJSON = new DataReaderImplJSON
     
     val expectedJSON = dataReaderJSON.readDataFast("recipe.json").lr
     val expectedCSV = dataReaderCSV.readDataSlow("other_recipe.csv").lr
     
     assert(expectedJSON!==expectedCSV)
     
  }
  
    // using strategy to read one file JSON and another CSV, with same content, verify equality
  test("Exercise 3. Test 4. - Verify strategy - Verify equality") {
      val expectedJSON = ReadRecipesStrategy.readRecipes("recipe.json")      
      val expectedCSV = ReadRecipesStrategy.readRecipes("recipe.csv")
       
      assert(expectedJSON.get.lr===expectedCSV.get.lr)      
    
  }
  
   // using strategy to read one file JSON and another CSV, with different content, verify inequality
  test("Exercise 3. Test 5. - Verify strategy - Verify inequality") {
      val expectedJSON = ReadRecipesStrategy.readRecipes("recipe.json")      
      val expectedCSV = ReadRecipesStrategy.readRecipes("other_recipe.csv")
       
      assert(expectedJSON.get.lr!==expectedCSV.get.lr)        
    
  }
  
  
}