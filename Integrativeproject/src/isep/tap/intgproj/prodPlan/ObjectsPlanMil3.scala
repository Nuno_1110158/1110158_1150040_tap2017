package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.model._

case class ObjectsPlanMil3 (producao:Production) {
  
  
  
  /**
   * Ponto de entrada para obter a lista "OrdemProduto" para escalonar
   * 
   * @param lista de ordens
   * @return lista de "OrdemProduto para escalonar
   */
  def ordersProdsTasksExecutar(ords:List[Order]):List[OrdemProduto]={
    
      val orderProdutoNumerado=ordersProdutosNumeradosTasks(ords)
      
      taskQuePodemSerProduzidas(orderProdutoNumerado)
    
  }

    
//-ordem de seq 1----------------------------------------------------------------------------------------------------- 
  def ordersProdutosNumeradosTasks(ords:List[Order]):List[OrdemProduto]= ords match{
    
    case Nil=> Nil
    
    case ord::Nil=> ordemToProdutos(ord):::ordersProdutosNumeradosTasks(Nil)
    
    case ord::ordxs=> ordemToProdutos(ord):::ordersProdutosNumeradosTasks(ordxs)    
    
  }
/*  */  

//- da ordem de seq 1-----------------------------------------------------------------------------------------------------  

  //---criar uma lista de produtos a produzir com base na Ordem (OrdemProduto)- com numeração do produto correcta------    

   /**
    * Auxilia a função "ordersProdutosNumeradosTasks" (anterior)
    * Criar uma estrutura Ordem-ProdutoCom Tasks e numerar o produto
    * 
    * @param uma ordem
    * @return lista de "OrdemProduto"
    */
   def ordemToProdutos(o:Order):List[OrdemProduto]={

  		def numprodutos(quant:Int,nprod:Int):List[OrdemProduto]= quant match{
  				
  				case 0 => Nil
  				case _ => val prodd=prodrefToproduto(o.prdref)
  						if(prodd!=null){
  						
  						val numberprod=prodd.copy( nump=nprod)  //altera a propriedade nump
  							
  						 OrdemProduto(o.id,numberprod)::numprodutos(quant-1,nprod+1)
  						} else{ Nil }
				
		
          		}
          	
        numprodutos(o.quantity.toInt,1)

  }  
   
    /**
    * Auxilia a função "ordemToProdutos" (anterior). A partir da lista "produtoComTasks"
    * 
    * @param a lista "produtoComTasks"
    * @return cada um dos "ProdutoComTasks" identificados
    */
  def prodrefToproduto(prodid:String):ProdutoComTasks={
  
  		produtoComTasks.find ({ x => x.id==prodid }).getOrElse(null)
  
  }  
   
   //exemplo   val ordemProdutos= ordemToProdutos(Order("ORD_1","PRD_1",2))
 
  
//fim da seq. 1 --------------------------------------------------------------------------------------------------  
 
//ordem de seq. 2 -------------------------------------------------------------------------------------------------
 //Lista de todos os produtos com objectos Task (e não refs de tasks)
  
  val produtoComTasks=productTodoTOproduto(producao.products)  
    
// da ordem de seq 2-----------------------------------------------------------------------------------------------------  
     /**
    * Tranforma uma lista de "ProductToDo" (com refs. para tasks) em "ProdutoComTasks"
    * substitui refs, de tasks por objectos do tipo Task
    * 
    * @param lista de "ProductToDo"
    * @return devolve lista de "ProdutoComTasks"
    */
    //colocar no produto o objecto Task e não a referência
  def productTodoTOproduto(ptd:List[ProductToDo]):List[ProdutoComTasks]={
  
  			
  			def produtosComtasks(ptd:List[ProductToDo]):List[ProdutoComTasks]= ptd match{
  			
  					case Nil=>Nil
  					case x::Nil=>ProdutoComTasks(x.id,x.name,splitprodutoNum(x.name),refstaskTotask(x.tskrefs))::produtosComtasks(Nil)
  					case x::xs=>ProdutoComTasks(x.id,x.name,splitprodutoNum(x.name),refstaskTotask(x.tskrefs))::produtosComtasks(xs)
  			
  			}
  
  		produtosComtasks(ptd)
  } 
  
   /**
   * Auxilia a função "productTodoTOproduto" (anterior)
   * tranforma Ref's de tasks em Objectos Task
   * 
   * @param Lista com referencias a tasks 
   * @return devolve Lista de tasks (Objectos)
   */
    def refstaskTotask(taskrefs:List[String]):List[Task]= taskrefs match{
 
     			case Nil=>Nil
     			case x::Nil => val task=producao.tasks.find ({ t => t.id==x })getOrElse(null)
     						if(task!=null) task::refstaskTotask(Nil) else refstaskTotask(Nil)
     					
     			case x::xs=> val task=producao.tasks.find ({ t => t.id==x })getOrElse(null)
     						if(task!=null) task::refstaskTotask(xs) else refstaskTotask(xs)
 	  }
  
// Fim seq 2------------------------------------------------------------------------------------------------------ 
  
// seq 3---------------------------------------------------------------------------------------------------------
    
     /**
   * Devove uma lista com os "OrdemProduto" que posem ser produzidos
   * 
   * @param uma lista de "OrdemProduto"
   * @return uma lista de "OrdemProduto", que pode ser mais reduzida se algum produto não poder ser produzido
   */
   def taskQuePodemSerProduzidas(ordprod:List[OrdemProduto]):List[OrdemProduto]={  
     
         //só os produtos que têm pelo menos uma ordem que pode ser produzida
         //Se num produto todas as ordens não poderem ser produzidas esse produto não pode ser produzido
        
     ordprod.collect { case x if (x.prod.possoSerProduzido(osRecursos.phys, osRecursos.hums)
                                  && x.prod.tasks.length>0)=>x }
    
   }
   
// fim seq 3------------------------------------------------------------------------------------------------------
   
// funções necessarias em varias situações--------------------------------------------------------
  
     /**
     * devolve o conjunto dos recursos fisicos e humanos
     */
    
    def osRecursos:Recursos={
      
        Recursos(producao.fResources,producao.hResources)
    }
   
   /**
    * Usa o "name="Product 2"" para retirar o numero para atribuir ao "nump" em ProdutoComTasks
    * Este numero é depois substituido pela numeração correcta 
    * 
    *  @param nome so produto
    *  @return devolve um inteiro (retira do nome)
    */
  def splitprodutoNum(s:String):Integer={
 
 		val sssplit=s.indexOf(" ")
 		val num=s.slice(sssplit+1, s.length()).trim()
 	
     	try {
            //Some(num.toInt)
            num.toInt
        }catch{
            case e: NumberFormatException => 0
        }
  }
  
  
  
}