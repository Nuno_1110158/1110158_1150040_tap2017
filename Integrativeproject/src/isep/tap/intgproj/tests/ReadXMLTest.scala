package isep.tap.intgproj.tests

import scala.xml.XML
import isep.tap.intgproj.model._
import isep.tap.intgproj.dataIO._
import isep.tap.intgproj.prodPlan._
import org.scalatest.FunSuite
import isep.tap.intgproj.dataIO.DataReaderImpXML

class ReadXMLTest extends FunSuite {
  
       val datareaderImpXML=new DataReaderImpXML
       
       val whatFile:String = System.getProperty("user.dir")+"/files/"+"ip_2017_in.xml"
       
       val xml=XML.loadFile(whatFile)
  
  test("Teste 1 carregamento de PhysicalResources...") {
 
       
       val expected= List(PhysicalResource("PRS_1","PRST 1"), PhysicalResource("PRS_2","PRST 1"), 
                                                  PhysicalResource("PRS_3","PRST 2"), PhysicalResource("PRS_4","PRST 3"), 
                                                  PhysicalResource("PRS_5","PRST 4"), PhysicalResource("PRS_6","PRST 4"), 
                                                  PhysicalResource("PRS_7","PRST 5"), PhysicalResource("PRS_8","PRST 5"), 
                                                  PhysicalResource("PRS_9","PRST 5"))
                                                  
              
       assert(datareaderImpXML.physicalResourcesList(xml)===expected)

  }  
  
  test("Teste 2 carregamento de Tasks...") {
    
      val espected=List(Task("TSK_1",100,List("PRST 1","PRST 2","PRST 3")), Task("TSK_2",80,List("PRST 4","PRST 5")), 
                                        Task("TSK_3",160,List("PRST 1","PRST 3","PRST 5")),Task("TSK_4",90,List("PRST 2","PRST 4")), 
                                        Task("TSK_5",60,List("PRST 1","PRST 2","PRST 5")),Task("TSK_6",85,List("PRST 3","PRST 4")),
                                        Task("TSK_7",145,List("PRST 2","PRST 1","PRST 3")),Task("TSK_8",35,List("PRST 5","PRST 4")),
                                        Task("TSK_9",140,List("PRST 5","PRST 1","PRST 3")),Task("TSK_10",45,List("PRST 1","PRST 5","PRST 2")))
                                        
        assert(datareaderImpXML.taskResourceList(xml)===espected)                                  
     
  }  
  
   test("Teste 3 carregamento de HumanResources...") {
     
     val espected=List(HumanResource("HRS_1","Antonio",List("PRST 1","PRST 2")), 
                                     HumanResource("HRS_2","Maria",List("PRST 1","PRST 3","PRST 4","PRST 5")), 
                                     HumanResource("HRS_3","Manuel",List("PRST 3","PRST 5")),
                                     HumanResource("HRS_4","Susana",List("PRST 2","PRST 4")),
                                     HumanResource("HRS_5","Joao",List("PRST 1","PRST 3","PRST 5")),
                                     HumanResource("HRS_6","Laura",List("PRST 2","PRST 3","PRST 4")))
                                     
       assert(datareaderImpXML.humanResourceList(xml)===espected)                                
     
   }
  
  test("Teste 4 carregamento de Products...") {
    
      val espected=List(ProductToDo("PRD_1","Product 1",List("TSK_1","TSK_3","TSK_5","TSK_10")),
                      ProductToDo("PRD_2","Product 2",List("TSK_1","TSK_2","TSK_3","TSK_4","TSK_5","TSK_10")),
                      ProductToDo("PRD_3","Product 3",List("TSK_2","TSK_3","TSK_6","TSK_7","TSK_9")), 
                      ProductToDo("PRD_4","Product 4",List("TSK_8","TSK_7","TSK_6","TSK_5","TSK_10")))
    
      assert(datareaderImpXML.productResourceList(xml)===espected)                
                      
  }
  
  test("Teste 5 carregamento de Orders...") {
    
      val espected= List(Order("ORD_1","PRD_1",1),Order("ORD_2","PRD_2",2), Order("ORD_3","PRD_3",3),Order("ORD_4","PRD_4",1))
    
      assert(datareaderImpXML.ordersResourceList(xml)===espected)
    
  }
  
  test("Teste 6 carregamento de Production...") {
    
     val physicalResources= List(PhysicalResource("PRS_1","PRST 1"), PhysicalResource("PRS_2","PRST 1"), 
                                                  PhysicalResource("PRS_3","PRST 2"), PhysicalResource("PRS_4","PRST 3"), 
                                                  PhysicalResource("PRS_5","PRST 4"), PhysicalResource("PRS_6","PRST 4"), 
                                                  PhysicalResource("PRS_7","PRST 5"), PhysicalResource("PRS_8","PRST 5"), 
                                                  PhysicalResource("PRS_9","PRST 5"))
                                                  
     val taskResources=List(Task("TSK_1",100,List("PRST 1","PRST 2","PRST 3")), Task("TSK_2",80,List("PRST 4","PRST 5")), 
                                        Task("TSK_3",160,List("PRST 1","PRST 3","PRST 5")),Task("TSK_4",90,List("PRST 2","PRST 4")), 
                                        Task("TSK_5",60,List("PRST 1","PRST 2","PRST 5")))       
                                        
     val humanResources=List(HumanResource("HRS_1","Antonio",List("PRST 1","PRST 2")), 
                                     HumanResource("HRS_2","Maria",List("PRST 1","PRST 3","PRST 4","PRST 5")), 
                                     HumanResource("HRS_3","Manuel",List("PRST 3","PRST 5")))  
                                     
     val productResources=List(ProductToDo("PRD_1","Product 1",List("TSK_1","TSK_3","TSK_5","TSK_10")),
                                ProductToDo("PRD_2","Product 2",List("TSK_1","TSK_2","TSK_3","TSK_4","TSK_5","TSK_10")),
                                ProductToDo("PRD_3","Product 3",List("TSK_2","TSK_3","TSK_6","TSK_7","TSK_9")), 
                                ProductToDo("PRD_4","Product 4",List("TSK_8","TSK_7","TSK_6","TSK_5","TSK_10")))                                
     
     val orderResources= List(Order("ORD_1","PRD_1",1),Order("ORD_2","PRD_2",2), Order("ORD_3","PRD_3",3),Order("ORD_4","PRD_4",1))     
     
     
     val espectedproduction=Production(physicalResources,taskResources,humanResources,productResources,orderResources)
     
     assert(datareaderImpXML.read("ip2_2017_in.xml").get===espectedproduction)
    
  }
  
  test("Teste 7  Production==None...") {
    
     assert(datareaderImpXML.read("ip2_2020_in.xml")===None)
  }
}