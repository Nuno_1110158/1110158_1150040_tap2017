package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.model._

case class DefinirEscalonamentoTasksMil3() {
  
  
  
/**
 * Quando terminar a lista "OrdemProduto" podem existir, ainda, tasks por executar (Lista intermedia)
 * a lista intermedia vai ser processada até não existirem mais tasks
 * 
 * @param recebe "ObjectosEscalonamento" para processar a lista intermedia
 * @return retorna o "ObjectosEscalonamento" com a list intermedia vazia
 */
  def ultimoEscalonamento(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento=objEscalonamento.listTaskScheIntermedia match{
    
    case Nil=>objEscalonamento
    case ts::Nil=>
                   val escalonar=produzirEscalonamento(objEscalonamento)
                   ultimoEscalonamento(escalonar)
    case ts::txs=> val escalonar=produzirEscalonamento(objEscalonamento)
                   ultimoEscalonamento(escalonar)
    
   
  }
  
  def instanteZero(opt:List[OrdemProduto],recs:Recursos):List[TasksSchedule]={
    
        def iniciar(opts:List[OrdemProduto],objsEscalonar:ObjectosEscalonamento,recss:Recursos):List[TasksSchedule]=opts match{
          
          case Nil=> //println("Ultimo escalar - "+objEscalonamento.listTaskScheIntermedia)  //lixo
            
                     val ultimoescalonar= ultimoEscalonamento(objsEscalonar)
                     ultimoescalonar.listTaskScheResultado 
          
          case tklist=> //println("task List - "+tklist.toString())
                      val novoOrdemProdutoRecursos=OrdemProdutoRecursos(tklist,List(),objsEscalonar.recs)
                      
                      //Lista de task que podem executar em paralelo
                      //val listatasksExecutar=tasksPodemSerExec(novoOrdemProdutoRecursos)
                      val listatasksExecutar=tasksPodemSerExec2(tklist,objsEscalonar.recs)   //atencão
                    
                      val recsRedusidos=recursosReduzidos(listatasksExecutar,objsEscalonar.recs)
                      
                      
                      println("Lista paralelo - "+listatasksExecutar.toString())  //lixo
                      
                      if(listatasksExecutar.length>0){
                        
											// três operações a serem feitos para a lista que vai prosseguir. 
											//1-remover a task da lista de tasks dp produto
											//2-tenho que substituir na lista total os produtos/tasks que já foram produzidos
											//3-remover os produtos com a lista de tasks vazia (o produto está produzido)   
                      
                      val removeTaskDeProduto=removerTaskDoProduto(listatasksExecutar)  //remover as tasks já executadas
                        
                      //tenho que substituir na lista total os produtos/tasks que já foram produzidos												
											val substituirProdutosNaListaTotal=substituirProdutos(removeTaskDeProduto,tklist)  
                      
											val removerProdutoJaProduzido=removerDaListaProdsSemTasks(substituirProdutosNaListaTotal)  //produto com lista de task vazia
											
											//aqui tenho as TasksSchedules
											val tksSchedule=dameTasksSchedules(listatasksExecutar, 0, 0, objsEscalonar.recs)
											
											//colocar a lista de tasksSchedule para executar e os recursos possiveis de utilizar
											val novoObjEscalonamento=objsEscalonar.copy(listTaskScheParaExec=tksSchedule,recs=recsRedusidos)
											
											// se for o inicio do processo										
											  
											  //  val inicioProcessObj= copiarTaskExecutarToIntermedia(novoObjEscalonamento) 

											    //iniciar(removerProdutoJaProduzido,inicioProcessObj,recss)
											  

											  //se não for o inicio do processo vai escalonar as tasks
											 // val escalonamentoProduzir=produzirEscalonamento(novoObjEscalonamento)
											  
											  val escalonamentoProduzir=escalonarIntermedia2(novoObjEscalonamento)
											  
											  iniciar(removerProdutoJaProduzido,escalonamentoProduzir,escalonamentoProduzir.recs)
											
                      
                      
                        
                      }else{
                        
                        val escalonamentoProduzir=escalonarIntermedia2(objsEscalonar)
                        
                        iniciar(opts,escalonamentoProduzir,escalonamentoProduzir.recs)
                      }
                      
                      
          
        }
        
        
        //criar o objecto "ObjectosEscalonamento" inicial
        val objEscalonar=ObjectosEscalonamento(List(),List(),List(),0,recs)
        
        iniciar(opt,objEscalonar,recs)
    
  }
  
 
  //escalonar tasksSchedules em função das novas tasksSchedule a executar
  //se não houver novas tasksSchedule a executar faz o escalonamento de um elemento da lista intermedia
  def produzirEscalonamento(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento={
    
      if(objEscalonamento.listTaskScheParaExec.length==0){
        
            //ordenar a lista intermedia
            val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objEscalonamento.listTaskScheIntermedia)
            
            //colocar taskschedule no fim lista resultado
            val novaListaresultado=objEscalonamento.listTaskScheResultado:::List(listaIntermediaOrdenar.head)
            
            //criar novo objecto com nova lista resultado
            val novaObjectoresultado=objEscalonamento.copy(listTaskScheIntermedia=listaIntermediaOrdenar, 
                                      listTaskScheResultado=novaListaresultado)
            
            //disponibilizar novos recursos (da task concluida)
            val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objEscalonamento.recs.phys
            val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objEscalonamento.recs.hums
            
            val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
            
            ObjectosEscalonamento(listaIntermediaOrdenar.tail,novaObjectoresultado.listTaskScheParaExec,
                novaObjectoresultado.listTaskScheResultado,novaObjectoresultado.lastEnd,novosRecursos)
            
            
      }else{
        
            escalonarIntermedia(objEscalonamento)            
            
      }
    
  }
  /* */
  
  def escalonarIntermedia2(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento={
    
    //ordenar a lista intermedia
    val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objEscalonamento.listTaskScheIntermedia)
    //Novo objecto com a lista ordenada
    val novoobjEscalonamento=objEscalonamento.copy(listTaskScheIntermedia=listaIntermediaOrdenar)
    
    def escalonar(objsEscalonar:ObjectosEscalonamento):ObjectosEscalonamento=
                    (objsEscalonar.listTaskScheIntermedia,objsEscalonar.listTaskScheParaExec) match{
      
    
        case (Nil,Nil)=>objsEscalonar   //talvez colocar aqui ultimo escalonar
        
        case (Nil,_)=> copiarTaskExecutarToIntermedia(objsEscalonar)  //copia la lista Exec para a intermedia erro se for end!=0
        
         case (tks::_,Nil)=>
          
                //colocar taskschedule no fim lista resultado
                val novaListaresultado=objsEscalonar.listTaskScheResultado:::List(tks)
                
                 //qual é o end da taskSchedule
                val novoStart=tks.end
                
                 //disponibilizar novos recursos (da task concluida)
                val novosRecursosPhy=tks.physResources:::objsEscalonar.recs.phys
                val novosRecursosHums=tks.humanResources:::objsEscalonar.recs.hums
                
                val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
                
                val novaListaintermedia=objsEscalonar.listTaskScheIntermedia.tail
                
                val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,Nil,novaListaresultado,novoStart,novosRecursos)
                
                novoObjectoEscalonamento
                
        case (tk::Nil,tks::Nil)=>
                                       
                //colocar taskschedule no fim lista resultado
                val novaListaresultado=objsEscalonar.listTaskScheResultado:::List(tks)
                
                //qual é o end da taskSchedule
                val novoStart=tks.end
                val novoEnd=novoStart+tk.end
                
                //disponibilizar novos recursos (da task concluida)
                val novosRecursosPhy=tks.physResources:::objsEscalonar.recs.phys
                val novosRecursosHums=tks.humanResources:::objsEscalonar.recs.hums
                
                val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
                
                //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
                val novoTS=tk.copy(start=novoStart, end=novoEnd)
                
                //adiciona ao fim da lista intermedia a nova taskSchedule
                val novaListaintermedia=objsEscalonar.listTaskScheIntermedia.tail:::List(novoTS)
                
                val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,Nil,novaListaresultado,novoEnd,novosRecursos)
                
                escalonar(novoObjectoEscalonamento)
       /* 
       
        */
    
        case (tk::Nil,tks::tksx)=>
          
            //colocar taskschedule no fim lista resultado
                val novaListaresultado=objsEscalonar.listTaskScheResultado:::List(tks)
                
                //qual é o end da taskSchedule
                val novoStart=tks.end
                val novoEnd=novoStart+tk.end
                
                //disponibilizar novos recursos (da task concluida)
                val novosRecursosPhy=tks.physResources:::objsEscalonar.recs.phys
                val novosRecursosHums=tks.humanResources:::objsEscalonar.recs.hums
                
                val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
                
                //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
                val novoTS=tk.copy(start=novoStart, end=novoEnd)
                
                //adiciona ao fim da lista intermedia a nova taskSchedule
                val novaListaintermedia=objsEscalonar.listTaskScheIntermedia.tail:::List(novoTS)
                
                val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,Nil,novaListaresultado,novoEnd,novosRecursos)
                
                escalonar(novoObjectoEscalonamento)
          
        case (tk::tkx,tks::Nil)=>        
          
           //colocar taskschedule no fim lista resultado
                val novaListaresultado=objsEscalonar.listTaskScheResultado:::List(tks)
                
                //qual é o end da taskSchedule
                val novoStart=tks.end
                val novoEnd=novoStart+tk.end
                
                //disponibilizar novos recursos (da task concluida)
                val novosRecursosPhy=tks.physResources:::objsEscalonar.recs.phys
                val novosRecursosHums=tks.humanResources:::objsEscalonar.recs.hums
                
                val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
                
                //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
                val novoTS=tk.copy(start=novoStart, end=novoEnd)
                
                //adiciona ao fim da lista intermedia a nova taskSchedule
                val novaListaintermedia=objsEscalonar.listTaskScheIntermedia.tail:::List(novoTS)
                
                val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,tkx,novaListaresultado,novoEnd,novosRecursos)
                
                escalonar(novoObjectoEscalonamento)
                
        case (tk::tkx,tks::tksx)=>
              
                //colocar taskschedule no fim lista resultado
                val novaListaresultado=objsEscalonar.listTaskScheResultado:::List(tks)
                
                //qual é o end da taskSchedule
                val novoStart=tks.end
                val novoEnd=novoStart+tk.end
                
                //disponibilizar novos recursos (da task concluida)
                val novosRecursosPhy=tks.physResources:::objsEscalonar.recs.phys
                val novosRecursosHums=tks.humanResources:::objsEscalonar.recs.hums
                
                val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
                
                //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
                val novoTS=tk.copy(start=novoStart, end=novoEnd)
                
                //adiciona ao fim da lista intermedia a nova taskSchedule
                val novaListaintermedia=objsEscalonar.listTaskScheIntermedia.tail:::List(novoTS)
                
                val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,tkx,novaListaresultado,novoEnd,novosRecursos)
                
                escalonar(novoObjectoEscalonamento)
            
    }
    
    escalonar(novoobjEscalonamento)
    
  }
  

  //ajuda a função "produzirEscalonamento" (anterior) a fazer o escalonamento intermedio
  //em função das novas tasksSchedules a executar
  def escalonarIntermedia(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento=objEscalonamento.listTaskScheParaExec match{
            
    
    case Nil=>objEscalonamento
    
    case ts::Nil=> 
            
          if(objEscalonamento.listTaskScheIntermedia.length>1){
      //ordenar a lista intermedia
            val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objEscalonamento.listTaskScheIntermedia)
            
            //colocar taskschedule no fim lista resultado
            val novaListaresultado=objEscalonamento.listTaskScheResultado:::List(listaIntermediaOrdenar.head)
            
            //qual é o end da taskSchedule
            val novoStart=listaIntermediaOrdenar.head.end
            val novoEnd=novoStart+ts.end
            
            //disponibilizar novos recursos (da task concluida)
            val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objEscalonamento.recs.phys
            val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objEscalonamento.recs.hums
            
            val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
            
            //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
            val novoTS=ts.copy(start=novoStart, end=novoEnd)
            
            //adiciona ao fim da lista intermedia a nova taskSchedule
            val novaListaintermedia=listaIntermediaOrdenar.tail:::List(novoTS)
            
            val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,Nil,novaListaresultado,novoEnd,novosRecursos)
            
            escalonarIntermedia(novoObjectoEscalonamento)
            
          }else{
            
                   //qual é o end da taskSchedule
                  val novoStart=objEscalonamento.lastEnd
                  val novoEnd=novoStart+ts.end
                  //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
                  val novoTS=ts.copy(start=novoStart, end=novoEnd)
                  
                   //adiciona ao fim da lista intermedia a nova taskSchedule
                  val novaListaintermedia=objEscalonamento.listTaskScheIntermedia:::List(novoTS)
                  
                 val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,Nil,
                                           objEscalonamento.listTaskScheResultado,novoEnd,objEscalonamento.recs) 
                                           
                                           
             escalonarIntermedia(novoObjectoEscalonamento)                               
          }
            
 case ts::tsx=>
           
           if(objEscalonamento.listTaskScheIntermedia.length>1){
             //ordenar a lista intermedia
            val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objEscalonamento.listTaskScheIntermedia)
            
            //colocar taskschedule no fim lista resultado
            val novaListaresultado=objEscalonamento.listTaskScheResultado:::List(listaIntermediaOrdenar.head)
            
            //qual é o end da taskSchedule
            val novoStart=listaIntermediaOrdenar.head.end
            val novoEnd=novoStart+ts.end
            
            //disponibilizar novos recursos (da task concluida)
            val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objEscalonamento.recs.phys
            val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objEscalonamento.recs.hums
            
            val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
            
            //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
            val novoTS=ts.copy(start=novoStart, end=novoEnd)
            
            //adiciona ao fim da lista intermedia a nova taskSchedule
            val novaListaintermedia=listaIntermediaOrdenar.tail:::List(novoTS)
            
            val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,tsx,novaListaresultado,novoEnd,novosRecursos)
            
            escalonarIntermedia(novoObjectoEscalonamento)   
            
           }else{
             
               //qual é o end da taskSchedule
                  val novoStart=objEscalonamento.lastEnd
                  val novoEnd=novoStart+ts.end
                  //copia a taskSchedule e altera o start e end para adicionar a lista intermedia
                  val novoTS=ts.copy(start=novoStart, end=novoEnd)
                  
                   //adiciona ao fim da lista intermedia a nova taskSchedule
                  val novaListaintermedia=objEscalonamento.listTaskScheIntermedia:::List(novoTS)
                  
                 val novoObjectoEscalonamento=ObjectosEscalonamento(novaListaintermedia,tsx,
                                           objEscalonamento.listTaskScheResultado,novoEnd,objEscalonamento.recs) 
                                           
                                           
             escalonarIntermedia(novoObjectoEscalonamento)          
           }
        
  }

   /**
   * 
   * Recebe um objecto de "OrdemProdutoRecursos" que estão prontos a executar
   * escolhe quais podem ser executadas ao mesmo tempo
   * devolve um objecto de "OrdemProdutoRecursos" com a lista de "OrdemProduto" para executar ao mesmo tempo
   * com os recursos reduzidos (alguns estão a ser usados)
   * 
   * @param  "OrdemProdutoRecursos" 
   * @return "OrdemProdutoRecursos" 
   */
  def tasksPodemSerExec(opRecs:OrdemProdutoRecursos):OrdemProdutoRecursos= opRecs.opList match{
  
  		case Nil=>opRecs
  		case tk::Nil=> val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		          
  		        if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(opRecs.recs.phys, opRecs.recs.hums)) {
  		  
  							  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(opRecs.recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, opRecs.recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, opRecs.recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, opRecs.recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //val novoOpRecs=OrdemProdutoRecursos(Nil,opRecs.opListParalelo:::List(tk),recursosRed)
  							  
  							  val novoOpRecs=OrdemProdutoRecursos(Nil,tk::opRecs.opListParalelo,recursosRed)
  							  
  							  
  							  tasksPodemSerExec(novoOpRecs)
  							  
  							}else{ opRecs}
  							
  		case tk::tks=>val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		  
  		          if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(opRecs.recs.phys, opRecs.recs.hums)) {
  		              
  		            val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(opRecs.recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, opRecs.recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								
  								
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, opRecs.recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, opRecs.recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //val novoOpRecs=OrdemProdutoRecursos(tks,opRecs.opListParalelo:::List(tk),recursosRed)
  							  
  							  val novoOpRecs=OrdemProdutoRecursos(tks,tk::opRecs.opListParalelo,recursosRed)
  							  
  							  tasksPodemSerExec(novoOpRecs)
  						
  								
  							}else{ opRecs}

  } 
  
   //remover task da lista de tasks
   /**
    * Remove as tasks já produzidas da lista de tasks de cada produto
    * 
    * @param lista de "OrdemProduto" para retirar a task (head) do produto
    * @return Lista de "OrdemProduto" só com as (tails) do produto 
    */
  def removerTaskDoProduto(ordProExec:List[OrdemProduto]):List[OrdemProduto]= ordProExec match {
  
  		case Nil=>Nil
  		
  		case ts::Nil=>
  									val removetaskHead1=ts.prod.tasks.tail   // tail da lista de tasks
  									
  									val removetaskHead2=ts.prod.copy(tasks=removetaskHead1)  //copiar o produto com novalista de tasks
  									
  									val removetaskHead3=ts.copy( prod=removetaskHead2)  //copiar o "OrdemProduto" com novo produto
  									
  									removetaskHead3::removerTaskDoProduto(Nil)   //Novo "OrdemProduto" sem uma head task
  									
  	 case ts::tks=>
  									val removetaskHead1=ts.prod.tasks.tail   // tail da lista de tasks
  									
  									val removetaskHead2=ts.prod.copy(tasks=removetaskHead1)  //copiar o produto com novalista de tasks
  									
  									val removetaskHead3=ts.copy( prod=removetaskHead2)  //copiar o "OrdemProduto" com novo produto
  									
  									removetaskHead3::removerTaskDoProduto(tks)   //Novo "OrdemProduto" sem uma head task
  
  } 
  
 
  /**
   * Depois de remover as tasks já produzidas é necessário substituir os produtos com a lista
   * de tasks reduzida, na lista total de  "OrdemProduto" (tklist)
   * 
   * @param recebe a lista com os produtos alterados e a lista total de "OrdemProduto"
   * @return nova lista total de "OrdemProduto"
   */
 def substituirProdutos(list1:List[OrdemProduto],listTotal:List[OrdemProduto]):List[OrdemProduto]={
 
 				
 				 def substituirProduto(list1:List[OrdemProduto],listTotal:List[OrdemProduto]):List[OrdemProduto]= list1 match{
 				 
 				 		case Nil=>listTotal
 				 		case pr::Nil=> val ppp=substituirUmProduto(pr,listTotal)
 				 									substituirProduto(Nil,ppp)
 				 		case pr::prs=> val ppp=substituirUmProduto(pr,listTotal)
 				 									substituirProduto(prs,ppp)
 				 }
 
 			substituirProduto(list1,listTotal)
  } 
   
 /**
  * auxilia a função "substituirProdutos" na substituição de cada produto
  * 
  * @param recebe um produto e se o encontrar na lista total substitui-o
  * @return nova lista "OrdemProduto" com o produto substituido, se encontrado
  */
  def substituirUmProduto(orderprod:OrdemProduto,listTotal:List[OrdemProduto]):List[OrdemProduto]={
 
 			val lisTotal=	listTotal.map{ p=> if(p.ordId==orderprod.ordId && p.prod.nump==orderprod.prod.nump) orderprod else p }
 			
 			lisTotal
 } 
  
  
     /**
   * 
   * Recebe uma lista de "OrdemProduto" para devolver uma lista de "TasksSchedule" da lista de entrada.
   * A lista de entrada contém as "OrdemProduto" que podem ser executadas ao mesmo tempo
   * 
   * @param lista de "OrdemProduto" que podem ser executadas ao mesmo tempo
   * @return devolve lista de "TasksSchedule"
   */
  def dameTasksSchedules(opt:List[OrdemProduto],start:Int,end:Int,recs:Recursos):List[TasksSchedule]= opt match{
  
  		case Nil=>Nil
  		case tk::Nil=> 
  		               val taskphyresources=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task  									 
  									 val taskhumanresources= tk.prod.tasks.head.numRechuman(taskphyresources, recs.hums) //Lista de recursos humanos para os recursos fisicos desta task
  										
  	                //reduzir a lista de recursos
    								val phyRecsAtribuidosRed=removerDaLista(taskphyresources, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
    								val humRecsAtribuidosRed=removerDaLista(taskhumanresources, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  		               
  		               val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  										
  										TasksSchedule(tk.ordId,tk.prod.nump,tk.prod.tasks.head.id,start,tk.prod.tasks.head.time+start,
  										taskphyresources,taskhumanresources)::dameTasksSchedules(Nil,start,end,recursosRed)
  	
  	  case tk::tks=> val taskphyresources=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  									 val taskhumanresources= tk.prod.tasks.head.numRechuman(taskphyresources, recs.hums) //Lista de recursos humanos para os recursos fisicos desta task
  										
                      //reduzir a lista de recursos
      								val phyRecsAtribuidosRed=removerDaLista(taskphyresources, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
      								val humRecsAtribuidosRed=removerDaLista(taskhumanresources, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
    		               
  		                val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  									 
  									 
  										TasksSchedule(tk.ordId,tk.prod.nump,tk.prod.tasks.head.id,start,tk.prod.tasks.head.time+start,
  										taskphyresources,taskhumanresources)::dameTasksSchedules(tks,start,end,recursosRed)
  
  }
  
  
 // funções de ajuda -----------------------------------------------------------------------------------------------------
  
  /**
   * Usada para simplesmente copiar no instante zero as tasksschedule a executar para a lista intermedia
   * colocando o conteudo das listas em - Lista intermedia com dados, lista tasksschedule a executar vazia (Nil)
   * Usada na função "definirEscalonamento"
   * 
   * @param ObjectosEscalonamento
   * @return ObjectosEscalonamento alterado
   */
  def copiarTaskExecutarToIntermedia(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento={
    
    if(objEscalonamento.lastEnd==0){
      
      ObjectosEscalonamento(objEscalonamento.listTaskScheParaExec,Nil,objEscalonamento.listTaskScheResultado,
                           objEscalonamento.lastEnd,objEscalonamento.recs)
      
    }else{
      
      //se a lista intermedia estiver vazia coloca o start=lastEnd em todas as tasksSchedule da lista exec
        val novaListaExec=objEscalonamento.listTaskScheParaExec.map { x => x.copy(start=objEscalonamento.lastEnd) }
        
         ObjectosEscalonamento(novaListaExec,Nil,objEscalonamento.listTaskScheResultado,
                           objEscalonamento.lastEnd,objEscalonamento.recs)
      
    }
     
    
  }
  
  /**
   * Ordenação das lista de "TasksSchedule" pela propriedade "end" com ordem decrescente.
   * Uma outra forma de fazer o mesmo é fazer o mix do "trait Ordered" e definir o método "compare"
   * 
   * @param lista de "TasksSchedule"
   * @return lista de "TasksSchedule" ordenada de acordo
   */
  def taskScheduleOrdenadaZA(tskSche:List[TasksSchedule]):List[TasksSchedule]={
    
        tskSche.sortWith(_.end < _.end)
    
  }
  
    /**
     * remove da "listaTotal" os elementos da "subLista", de qualquer tipo
     * é usada em várias funções
     */
    def removerDaLista(subLista:List[Any],listaTotal:List[Any]):List[Any]={
    
    		listaTotal.filterNot(subLista.contains(_))
    }   
    
   /**
    * Remover da lista  "OrdemProduto" produtos sem tasks, portanto já produzidos.
    * 
    * @param lista de "OrdemProduto" para ser verificada
    * @return lista de "OrdemProduto" depois de verificada
    */
    def removerDaListaProdsSemTasks(subLista:List[OrdemProduto]):List[OrdemProduto]={
    
    		val sublist=subLista.filterNot { x => x.prod.tasks==Nil }
    		
    		sublist
    }
    
 //----------------------------------------------------------------------------------------
     
    /**
   * Auxilia a função "instanteZero" (anterior)
   * Recebe uma lista de "OrdemProduto" que estão prontos a executar
   * escolhe quais podem ser executadas ao mesmo tempo
   * devolve uma lista de "OrdemProduto" (tasks) 
   * 
   * @param lista de "OrdemProduto" que estão para executar
   * @return lista de "OrdemProduto" executadas ao mesmo tempo
   */
  def tasksPodemSerExec2(opt:List[OrdemProduto],recs:Recursos):List[OrdemProduto]= opt match{
  
  		case Nil=>Nil
  		case tk::Nil=> val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		          
  		        if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)) {
  		  
  							  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //println("Recursos reduzidos - "+ recursosRed.toString())
  							  tk::tasksPodemSerExec2(Nil,recursosRed)
  							  
  							}else{ Nil}
  							
  		case tk::tks=>val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		  
  		          if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)) {
  		              
  		            val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //println("Recursos reduzidos - "+ recursosRed.toString())
  							  tk::tasksPodemSerExec2(tks,recursosRed)
  						
  								
  							}else{ tasksPodemSerExec2(tks,recs)}

  } 
  
  //recursos reduzidos 
  def recursosReduzidos(opt:List[OrdemProduto],recs:Recursos):Recursos=opt match{
    
    case Nil=>recs
    case tk::Nil=>
        					
                  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							 recursosReduzidos(Nil,recursosRed)
  							  
    case tk::tks=>						  
      
                  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  recursosReduzidos(tks,recursosRed)
    
  }
  
}