package isep.tap.intgproj.prodPlan

import scala.xml
import isep.tap.intgproj.model._


case class PlanProduction (physicalResources:List[PhysicalResource],
                      taskResources:List[Task],
                      humanResources:List[HumanResource],
                      productResources:List[ProductToDo]) {
  
  def orderIntoSchedule(l : List[Order], timer: Int) : List[TasksSchedule] = l match {
    case x::xs => {
      if(productResources.find(p => p.id == x.prdref) == None) {
        Nil;
      } else {
        val ts = getOrderQuantity(x, productResources.find(p => p.id == x.prdref).get.tskrefs,timer, x.quantity, 1)
        if(ts.isEmpty) {
          ts
        } else {
          ts:::orderIntoSchedule(xs, ts.last.end)
        }
      }
    }
    case Nil => Nil
  }
    
  def getOrderQuantity(o: Order, p: List[String], timer: Int, quantity: Int, productNumber: Int) : List[TasksSchedule] = quantity match {
    case 0 => Nil
    case _ => {
      if(productResources.find(p => p.id == o.prdref) == None) {
        Nil
      } else {
        val ts = getProducts(o, productResources.find(p => p.id == o.prdref).get.tskrefs, timer, productNumber)
        if(ts.isEmpty) {
          ts
        } else {
          ts:::getOrderQuantity(o,p,ts.last.end,quantity-1, productNumber + 1);
        }
      }
    }
  }
    
  def getProducts(o: Order, p: List[String], timer: Int, productNumber: Int) : List[TasksSchedule] = p match {
    case x::xs => {
      if(taskResources.find(t => t.id == x) == None) {
        Nil
      } else {
        val ts = getTasks(o, p, taskResources.find(t => t.id == x).get, taskResources.find(t => t.id == x).get.physicRtype,Nil,Nil,timer, productNumber)
        if(ts.isEmpty) {
          ts
        } else {
          ts:::getProducts(o,p.tail, ts.last.end, productNumber)
        }
      }
    }
    case Nil => Nil
  }
  
    def getTasksAux(o: Order, p: List[String], t: Task, physicRType: List[String], prl: List[PhysicalResource], hl: List[HumanResource]) : List[Resource] = physicRType match {
    case x::xs => {
      if(physicalResources.find(p => p.typeR == x) == None) {
        Nil
      } else {
        val resource = getPhysicalResource(o, p, t, physicalResources.find(p => p.typeR == x).get,prl,hl)
        resource:::getTasksAux(o,p,t,physicRType.tail,prl:::resource.map(r => r.physicalResource),hl:::resource.map(r => r.operator))
      }
    }
    case Nil => Nil
  }
  
  def getTasks(o: Order, p: List[String], t: Task, physicRType: List[String], prl: List[PhysicalResource], hl: List[HumanResource], timer: Int, productNumber: Int) : List[TasksSchedule] = physicRType match {
    case x::xs => {
      if(physicalResources.find(p => p.typeR == x) == None){
        Nil
      } else {
        val resource = getPhysicalResource(o, p, t, physicalResources.find(p => p.typeR == x).get,prl,hl)
        val resourceNew = resource:::getTasksAux(o,p,t,physicRType.tail,resource.map(r => r.physicalResource),resource.map(r => r.operator))
        val ts = new TasksSchedule(o.id,productNumber,t.id,timer,timer+t.time,resourceNew.map(r => r.physicalResource),resourceNew.map(r => r.operator));
        val tsList : List[TasksSchedule] = List(ts)
        tsList
      }
    }
    case Nil => Nil
  }
  
  def getPhysicalResource(o: Order, p: List[String], t: Task, pr: PhysicalResource, prl: List[PhysicalResource], hl: List[HumanResource]) : List[Resource] = {
    val physicalResourceObj = physicalResources.find(p => p.typeR == pr.typeR).get;
    val humanResourcesAvailable = humanResources.filter(h => !hl.contains(h));
    if(humanResourcesAvailable.find(h => h.handeltype.indexOf(physicalResourceObj.typeR) != -1) == None) {
      val resource = new Resource(physicalResourceObj, new HumanResource("","",List())) :: Nil;
      resource
    } else {
      val humanResource = humanResourcesAvailable.find(h => h.handeltype.indexOf(physicalResourceObj.typeR) != -1).get;
      val resource = new Resource(physicalResourceObj,humanResource) :: Nil;
      resource
    }
  }
    
}