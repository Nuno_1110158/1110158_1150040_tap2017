package isep.tap.intgproj.prodPlan

import isep.tap.intgproj.model._

case class EscalonamentoTasksPlanMil3() {
  
  
    
  /**
   * Entrada para escalonar Mil3
   */
  def escalonarTasksMil3(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2={
      
        //iniciar aqui o escalonamento
        val objectoFinal=iniciar(objsEscalonar)
        
        val escalonarUltimaListaLimbo=ultimoEscalonamentoListaLimbo(objectoFinal)
        
        val escalonarUltimaListaIntermedia=ultimoEscalonamentoListaIntermedia(escalonarUltimaListaLimbo)
          
        val novalistaResultadoSort=taskScheduleOrdenadaZA(escalonarUltimaListaIntermedia.listTaskScheResultado)
        
        val novoEscalonarUltimaListaIntermedia=escalonarUltimaListaIntermedia.copy(listTaskScheResultado=novalistaResultadoSort)
        
        novoEscalonarUltimaListaIntermedia
        //escalonarUltimaListaIntermedia    
          
    }
     
    
    def iniciar(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2=objsEscalonar.opList match{
      
      case Nil=>objsEscalonar
      
      case taskList=>        
         //Lista de task que podem executar em paralelo
        val listatasksExecutar=tasksPodemSerExec(objsEscalonar.opList,objsEscalonar.recs)
        
        //tenho que passar, para criar as tasksSchedule, os recursos antes de serem reduzidos
        val recursosParaTaskSchedule=objsEscalonar.recs
        
        //retirar recursos em uso
        val recsRedusidos=recursosReduzidos(listatasksExecutar,objsEscalonar.recs)
        
        if(listatasksExecutar.length>0){
          
            // três operações a serem feitos para a lista que vai prosseguir. 
    				//1-remover a task da lista de tasks dp produto
    				//2-tenho que substituir na lista total os produtos/tasks que já foram produzidos
    				//3-remover os produtos com a lista de tasks vazia (o produto está produzido)  
              
             val removeTaskDeProduto=removerTaskDoProduto(listatasksExecutar)  //remover as tasks já executadas
                            
             //tenho que substituir na lista total os produtos/tasks que já foram produzidos												
    				val substituirProdutosNaListaTotal=substituirProdutos(removeTaskDeProduto,objsEscalonar.opList)  
            
    				// nova lista de OrdemProduto - remover produto com lista de task vazia           
    				val novaListaOrdemProduto=removerDaListaProdsSemTasks(substituirProdutosNaListaTotal)   
    				
    			  //aqui tenho as TasksSchedules
						val tksSchedule=dameTasksSchedules(listatasksExecutar, 0, 0, recursosParaTaskSchedule)
						
						//colocar a lista de tasksSchedule para executar e os recursos possiveis de utilizar						
						val novoObjEscalonamento=objsEscalonar.copy(opList=novaListaOrdemProduto, listTaskScheParaExec=tksSchedule,
						     recs=recsRedusidos)
						
						//verifica se é o inicio do processo
						if(novoObjEscalonamento.listTaskScheIntermedia.length==0 && novoObjEscalonamento.listTaskScheParaExec.length>0 
                    && novoObjEscalonamento.listLimbo.length==0 && novoObjEscalonamento.listTaskScheResultado.length==0){
						  
						      //simplesmente copiar as tasks da lista exec para a lista intermedia       
                  //val novoObjectosEscalonamentoV2=novoObjEscalonamento.copy(listTaskScheIntermedia=novoObjEscalonamento.listTaskScheParaExec) 
                  val novoObjectosEscalonamentoV2=novoObjEscalonamento.copy(listTaskScheIntermedia=novoObjEscalonamento.listTaskScheParaExec,
                                                                            listTaskScheParaExec=Nil)
                  
						      iniciar(escalonarUmaTaskInicioProcesso(novoObjectosEscalonamentoV2))				      
						      
						}else{
						     
						    //não é o inicio
						    iniciar(seHouverTasksParaexec(novoObjEscalonamento))  	
						  
						} //fim else 
						
						
						
           //fim if listatasksExecutar
        }else{
          
            iniciar(seNaoHouverTasksParaExec(objsEscalonar))  
        }
        
      
    }
  
 
    /**
     * Por cada elemento da lista executar, se houver elementos na lista Limbo, 
     * a cada taskSchedule da lista exec atribuir o end da taskSchedule da lista Limbo como novo start
     * passar a taskSchedule da lista exec para a lista intermedia
     * passar a taskSchedule da lista Limbo para a lista resultado 
     */
    def seHouverTasksParaexec(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2={
      
          def taskEscalonar(objsEsc:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2= objsEsc.listTaskScheParaExec match{
            
            case Nil=>objsEsc
            case tk::Nil=> 
              
                if(objsEsc.listLimbo.length>0){
                  
                  //qual é o end da taskSchedule
                  val novoStart=objsEsc.listLimbo.head.end
                  val novoEnd=novoStart+tk.end  
                  
                  //task com novo start e novo end
                  val novoTkS=tk.copy(start=novoStart, end=novoEnd)
                  
                  //aqui verifica se existe uma task da mesma ordem, nº produto e task anterior
                  //verifica se o start anterior é inferior ao fim da task existente na lista resultado
                  
                  val novoTkS2=verificaStartTaskAnterior(novoTkS,objsEsc.listTaskScheResultado)
                  
                  val novoTkS3=if(novoTkS==novoTkS2){
                    
                      val verificaAnterior=verificaEndTaskAnterior(novoTkS2,objsEsc)
                      verificaAnterior
                    
                  }else{
                    
                    novoTkS2
                  }
                  
                  //adicionar nova task a lista intermadia
                  val novaListaIntermedia=objsEsc.listTaskScheIntermedia:::List(novoTkS3)
                  
                  //passar a task da lista limbo para a lista resultado
                  //val novaListaResultado=objsEsc.listTaskScheResultado:::List(objsEsc.listLimbo.head)
                  
                  //ordenar a lista intermedia
                  val novaListaIntermediaOrdenada=taskScheduleOrdenadaZA(novaListaIntermedia)
                  
                  //aqui tenho um novo objecto ObjectosEscalonamentoV2
                  val novoObjectosEscalonamentoV2=objsEsc.copy(listTaskScheIntermedia=novaListaIntermediaOrdenada,
                      listTaskScheParaExec=Nil)            
                 
                  taskEscalonar(novoObjectosEscalonamentoV2) 
              
                }else{
                     
                    if(objsEsc.listTaskScheIntermedia.length>0){
                    //vai escalunar uma task da lista intermedia para a lista limbo
                      taskEscalonar(escalonarUmaTaskInicioProcesso(objsEsc))
                   
                    }else{
                      
                      println("ERRO - Aqui não é suposto a lista Intermedia estar Vazia!!!!")
                      
                      objsEsc
                    }
                }
                    
            case tk::tks=>      
                 
                if(objsEsc.listLimbo.length>0){
                    
                    //qual é o end da taskSchedule
                    val novoStart=objsEsc.listLimbo.head.end
                    val novoEnd=novoStart+tk.end  
                    
                    //task com novo start e novo end
                    val novoTkS=tk.copy(start=novoStart, end=novoEnd)
                    
                    //adicionar nova task a lista intermadia
                    val novaListaIntermedia=objsEsc.listTaskScheIntermedia:::List(novoTkS)
                    
                    //passar a task da lista limbo para a lista resultado
                    //val novaListaResultado=objsEsc.listTaskScheResultado:::List(objsEsc.listLimbo.head)
                    
                    //ordenar a lista intermedia
                    val novaListaIntermediaOrdenada=taskScheduleOrdenadaZA(novaListaIntermedia)
                    
                    //aqui tenho um novo objecto ObjectosEscalonamentoV2
                    val novoObjectosEscalonamentoV2=objsEsc.copy(listTaskScheIntermedia=novaListaIntermediaOrdenada,
                        listTaskScheParaExec=tks)            
                   
                    taskEscalonar(novoObjectosEscalonamentoV2) 
                
                  }else{
                       
                      if(objsEsc.listTaskScheIntermedia.length>0){
                      //vai escalunar uma task da lista intermedia para a lista limbo
                        taskEscalonar(escalonarUmaTaskInicioProcesso(objsEsc))
                     
                      }else{
                        
                        println("ERRO - Aqui não é suposto a lista Intermedia estar Vazia!!!!")
                        
                        objsEsc
                      }
                  }
                        
          } //fim 2ª func
          
          taskEscalonar(objsEscalonar)
    }

  /**
   *  se não houver tasks para executar em paralelo
   *  colocar taskSchedule da lista Limbo para a lista resultado
   *  escalonar uma taskSchedule da lista intermedia para a Lista limbo
   *  e libertar recursos  
   */
  def seNaoHouverTasksParaExec(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2={
    
        
        //passar task da lista limbo para a lista resultado (em principio não há problema se a lista limbo estiver vazia)
        //val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(objsEscalonar.listLimbo.head)
        
        if(objsEscalonar.listTaskScheIntermedia.length>0){
          
          if(objsEscalonar.listLimbo.length>0){
            
              //passar task da lista limbo para a lista resultado (em principio não há problema se a lista limbo estiver vazia)
              val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(objsEscalonar.listLimbo.head)
          
              //ordenar a lista intermedia
              val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objsEscalonar.listTaskScheIntermedia)
              
              val tailListaIntermedia=objsEscalonar.listTaskScheIntermedia.tail
              
              val tailListaLimbo=objsEscalonar.listLimbo.tail
              
              //colocar taskschedule da lista intermedia no fim lista limbo
              val novaListaLimbo=tailListaLimbo:::List(listaIntermediaOrdenar.head)
              
             //disponibilizar novos recursos (da task concluida)
             val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objsEscalonar.recs.phys
             val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objsEscalonar.recs.hums
                
             val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
             
              //cópia de todos os objectos alterando os necessários
             //val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=tailListaIntermedia,
             //                           listLimbo=novaListaLimbo, recs=novosRecursos)
            
             val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=tailListaIntermedia, 
                                             listLimbo=novaListaLimbo, listTaskScheResultado=novaListaResultado, recs=novosRecursos)                           
                                        
                novoObjectosEscalonamentoV2
            
          }else{
            
               //passar task da lista limbo para a lista resultado (em principio não há problema se a lista limbo estiver vazia)
              //val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(objsEscalonar.listLimbo.head)
          
              //ordenar a lista intermedia
              val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objsEscalonar.listTaskScheIntermedia)
              
              val tailListaIntermedia=objsEscalonar.listTaskScheIntermedia.tail
              
              val tailListaLimbo=objsEscalonar.listLimbo.tail
              
              //colocar taskschedule da lista intermedia no fim lista limbo
              val novaListaLimbo=tailListaLimbo:::List(listaIntermediaOrdenar.head)
              
             //disponibilizar novos recursos (da task concluida)
             val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objsEscalonar.recs.phys
             val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objsEscalonar.recs.hums
                
             val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
             
              //cópia de todos os objectos alterando os necessários
             //val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=tailListaIntermedia,
             //                           listLimbo=novaListaLimbo, recs=novosRecursos)
            
             val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=tailListaIntermedia, 
                                             listLimbo=novaListaLimbo, recs=novosRecursos)                           
                                        
                novoObjectosEscalonamentoV2
            
          }
           
        }else{
          
            objsEscalonar
        }

    
  }
    
 
  /**
   * Se existirem taskSchedules na lista Limbo coloca-las na lista resultado 
   */
  def ultimoEscalonamentoListaLimbo(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2= 
                                  taskScheduleOrdenadaZA(objsEscalonar.listLimbo) match{
    
    case Nil=>objsEscalonar
    
    case tk::Nil=> 
      
          //aqui verifica se existe uma task da mesma ordem, nº produto e task anterior
         //verifica se o start anterior é inferior ao fim da task existente na lista resultado                  
         val novoTkS2=verificaStartTaskAnterior(tk,objsEscalonar.listTaskScheResultado)      
      
         val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(novoTkS2)
                  
         val novoObjectosEscalonamentoV2=objsEscalonar.copy(listLimbo=Nil, listTaskScheResultado=novaListaResultado)
         ultimoEscalonamentoListaLimbo(novoObjectosEscalonamentoV2)
                  
    case tk::tks=> 
      
           //aqui verifica se existe uma task da mesma ordem, nº produto e task anterior
           //verifica se o start anterior é inferior ao fim da task existente na lista resultado                  
           val novoTkS2=verificaStartTaskAnterior(tk,objsEscalonar.listTaskScheResultado)   
      
      
          val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(novoTkS2)
                  
          val novoObjectosEscalonamentoV2=objsEscalonar.copy(listLimbo=tks, listTaskScheResultado=novaListaResultado)
         ultimoEscalonamentoListaLimbo(novoObjectosEscalonamentoV2)              
  }
    
  /**
   * Se existirem taskSchedules na lista intermedia coloca-las na lista resultado 
   */
    def ultimoEscalonamentoListaIntermedia(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2= 
                                  taskScheduleOrdenadaZA(objsEscalonar.listTaskScheIntermedia) match{
    
    case Nil=>objsEscalonar
    
    case tk::Nil=> 
       
          //aqui verifica se existe uma task da mesma ordem, nº produto e task anterior
          //verifica se o start anterior é inferior ao fim da task existente na lista resultado                  
         val novoTkS2=verificaStartTaskAnterior(tk,objsEscalonar.listTaskScheResultado)
      
         val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(novoTkS2)
                  
         val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=Nil,listTaskScheResultado=novaListaResultado)
         ultimoEscalonamentoListaIntermedia(novoObjectosEscalonamentoV2)
                  
    case tk::tks=> 
      
         //aqui verifica se existe uma task da mesma ordem, nº produto e task anterior
         //verifica se o start anterior é inferior ao fim da task existente na lista resultado                  
         val novoTkS2=verificaStartTaskAnterior(tk,objsEscalonar.listTaskScheResultado)
      
      
         val novaListaResultado=objsEscalonar.listTaskScheResultado:::List(novoTkS2)
                  
         val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=tks,listTaskScheResultado=novaListaResultado)
         ultimoEscalonamentoListaIntermedia(novoObjectosEscalonamentoV2)              
  }
    
  /**
   * coloca na lista limbo uma taskSchedule da lista intermedia, libertar recursos
   */
  def escalonarUmaTaskInicioProcesso(objsEscalonar:ObjectosEscalonamentoV2):ObjectosEscalonamentoV2={
    
         //ordenar a lista intermedia
         val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objsEscalonar.listTaskScheIntermedia)
         
         //tail da lista intermedia ordenada
         val listaIntermediaOrdenarTail=listaIntermediaOrdenar.tail
         
         //colocar taskschedule no fim lista limbo
         val novaListaLimbo=objsEscalonar.listLimbo:::List(listaIntermediaOrdenar.head)
                        
         //disponibilizar novos recursos (da task concluida)
         val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objsEscalonar.recs.phys
         val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objsEscalonar.recs.hums
            
         val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
           
         //cópia de todos os objectos alterando os necessários
         val novoObjectosEscalonamentoV2=objsEscalonar.copy(listTaskScheIntermedia=listaIntermediaOrdenarTail,
                                                            listLimbo=novaListaLimbo, recs=novosRecursos)
         
         novoObjectosEscalonamentoV2                                                   
  }
    
    /**
   * Auxilia a função "instanteZero" (anterior)
   * Recebe uma lista de "OrdemProduto" que estão prontos a executar
   * escolhe quais podem ser executadas ao mesmo tempo
   * devolve uma lista de "OrdemProduto" (tasks) 
   * 
   * @param lista de "OrdemProduto" que estão para executar
   * @return lista de "OrdemProduto" executadas ao mesmo tempo
   */
  def tasksPodemSerExec(opt:List[OrdemProduto],recs:Recursos):List[OrdemProduto]= opt match{
  
  		case Nil=>Nil
  		case tk::Nil=> val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		          
  		        val possoSerProduzida= tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)         
  		 
  		        if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)) {
  		  
  							  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //println("Recursos reduzidos - "+ recursosRed.toString())
  							  tk::tasksPodemSerExec(Nil,recursosRed)
  							  
  							}else{ Nil}
  							
  		case tk::tks=>val taskHead=tk.prod.tasks.length   //verificar se a lista está vazia
  		          
  		          val possoSerProduzida= tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)         
  		
  		          if(taskHead>0 && tk.prod.tasks.head.possoSerProduzida(recs.phys, recs.hums)) {
  		              
  		            val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  //println("Recursos reduzidos - "+ recursosRed.toString())
  							  tk::tasksPodemSerExec(tks,recursosRed)
  						
  								
  							}else{ tasksPodemSerExec(tks,recs)}

  } 
     
    /**
   * 
   * Recebe uma lista de "OrdemProduto" para devolver uma lista de "TasksSchedule" da lista de entrada.
   * A lista de entrada contém as "OrdemProduto" que podem ser executadas ao mesmo tempo
   * 
   * @param lista de "OrdemProduto" que podem ser executadas ao mesmo tempo
   * @return devolve lista de "TasksSchedule"
   */
  def dameTasksSchedules(opt:List[OrdemProduto],start:Int,end:Int,recs:Recursos):List[TasksSchedule]= opt match{
  
  		case Nil=>Nil
  		case tk::Nil=> 
  		               val taskphyresources=tk.prod.tasks.head.numPhyRectask2(recs.phys) //Lista recursos fisicos desta task  									 
  									 val taskhumanresources= tk.prod.tasks.head.numRechuman(taskphyresources, recs.hums) //Lista de recursos humanos para os recursos fisicos desta task
  										
  	                //reduzir a lista de recursos
    								val phyRecsAtribuidosRed=removerDaLista(taskphyresources, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
    								val humRecsAtribuidosRed=removerDaLista(taskhumanresources, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  		               
  		               val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  										
  										TasksSchedule(tk.ordId,tk.prod.nump,tk.prod.tasks.head.id,start,tk.prod.tasks.head.time+start,
  										taskphyresources,taskhumanresources)::dameTasksSchedules(Nil,start,end,recursosRed)
  	
  	  case tk::tks=> val taskphyresources=tk.prod.tasks.head.numPhyRectask2(recs.phys) //Lista recursos fisicos desta task
  									 val taskhumanresources= tk.prod.tasks.head.numRechuman(taskphyresources, recs.hums) //Lista de recursos humanos para os recursos fisicos desta task
  										
                      //reduzir a lista de recursos
      								val phyRecsAtribuidosRed=removerDaLista(taskphyresources, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
      								val humRecsAtribuidosRed=removerDaLista(taskhumanresources, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
    		               
  		                val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  									 
  									 
  										TasksSchedule(tk.ordId,tk.prod.nump,tk.prod.tasks.head.id,start,tk.prod.tasks.head.time+start,
  										taskphyresources,taskhumanresources)::dameTasksSchedules(tks,start,end,recursosRed)
  
  }
  
//--funções de ajuda-------------------------------------------------------------------------------------------------------------------
    //remover task da lista de tasks
   /**
    * Remove as tasks já produzidas da lista de tasks de cada produto
    * 
    * @param lista de "OrdemProduto" para retirar a task (head) do produto
    * @return Lista de "OrdemProduto" só com as (tails) do produto 
    */
  def removerTaskDoProduto(ordProExec:List[OrdemProduto]):List[OrdemProduto]= ordProExec match {
  
  		case Nil=>Nil
  		
  		case ts::Nil=>
  									val removetaskHead1=ts.prod.tasks.tail   // tail da lista de tasks
  									
  									val removetaskHead2=ts.prod.copy(tasks=removetaskHead1)  //copiar o produto com novalista de tasks
  									
  									val removetaskHead3=ts.copy( prod=removetaskHead2)  //copiar o "OrdemProduto" com novo produto
  									
  									removetaskHead3::removerTaskDoProduto(Nil)   //Novo "OrdemProduto" sem uma head task
  									
  	 case ts::tks=>
  									val removetaskHead1=ts.prod.tasks.tail   // tail da lista de tasks
  									
  									val removetaskHead2=ts.prod.copy(tasks=removetaskHead1)  //copiar o produto com novalista de tasks
  									
  									val removetaskHead3=ts.copy( prod=removetaskHead2)  //copiar o "OrdemProduto" com novo produto
  									
  									removetaskHead3::removerTaskDoProduto(tks)   //Novo "OrdemProduto" sem uma head task
  
  } 
  
 
  /**
   * Depois de remover as tasks já produzidas é necessário substituir os produtos com a lista
   * de tasks reduzida, na lista total de  "OrdemProduto" (tklist)
   * 
   * @param recebe a lista com os produtos alterados e a lista total de "OrdemProduto"
   * @return nova lista total de "OrdemProduto"
   */
 def substituirProdutos(list1:List[OrdemProduto],listTotal:List[OrdemProduto]):List[OrdemProduto]={
 
 				
 				 def substituirProduto(list1:List[OrdemProduto],listTotal:List[OrdemProduto]):List[OrdemProduto]= list1 match{
 				 
 				 		case Nil=>listTotal
 				 		case pr::Nil=> val ppp=substituirUmProduto(pr,listTotal)
 				 									substituirProduto(Nil,ppp)
 				 		case pr::prs=> val ppp=substituirUmProduto(pr,listTotal)
 				 									substituirProduto(prs,ppp)
 				 }
 
 			substituirProduto(list1,listTotal)
  } 
   
 /**
  * auxilia a função "substituirProdutos" na substituição de cada produto
  * 
  * @param recebe um produto e se o encontrar na lista total substitui-o
  * @return nova lista "OrdemProduto" com o produto substituido, se encontrado
  */
  def substituirUmProduto(orderprod:OrdemProduto,listTotal:List[OrdemProduto]):List[OrdemProduto]={
 
 			val lisTotal=	listTotal.map{ p=> if(p.ordId==orderprod.ordId && p.prod.nump==orderprod.prod.nump) orderprod else p }
 			
 			lisTotal
 } 
  
  
  
  /**
 * Quando terminar a lista "OrdemProduto" podem existir, ainda, tasks por executar (Lista intermedia)
 * a lista intermedia vai ser processada até não existirem mais tasks
 * 
 * @param recebe "ObjectosEscalonamento" para processar a lista intermedia
 * @return retorna o "ObjectosEscalonamento" com a list intermedia vazia
 */
  def ultimoEscalonamento(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento=objEscalonamento.listTaskScheIntermedia match{
    
    case Nil=>objEscalonamento
    case ts::Nil=>
                   val escalonar=produzirEscalonamento(objEscalonamento)
                   ultimoEscalonamento(escalonar)
    case ts::txs=> val escalonar=produzirEscalonamento(objEscalonamento)
                   ultimoEscalonamento(escalonar)
    
   
  }   
  
  
    //escalonar tasksSchedules em função das novas tasksSchedule a executar
  //se não houver novas tasksSchedule a executar faz o escalonamento de um elemento da lista intermedia
  def produzirEscalonamento(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento={
           
            //ordenar a lista intermedia
            val listaIntermediaOrdenar=taskScheduleOrdenadaZA(objEscalonamento.listTaskScheIntermedia)
            
            //colocar taskschedule no fim lista resultado
            val novaListaresultado=objEscalonamento.listTaskScheResultado:::List(listaIntermediaOrdenar.head)
            
            //criar novo objecto com nova lista resultado
            val novaObjectoresultado=objEscalonamento.copy(listTaskScheIntermedia=listaIntermediaOrdenar, 
                                      listTaskScheResultado=novaListaresultado)
            
            //disponibilizar novos recursos (da task concluida)
            val novosRecursosPhy=listaIntermediaOrdenar.head.physResources:::objEscalonamento.recs.phys
            val novosRecursosHums=listaIntermediaOrdenar.head.humanResources:::objEscalonamento.recs.hums
            
            val novosRecursos=Recursos(novosRecursosPhy,novosRecursosHums)
            
            ObjectosEscalonamento(listaIntermediaOrdenar.tail,novaObjectoresultado.listTaskScheParaExec,
                novaObjectoresultado.listTaskScheResultado,novaObjectoresultado.lastEnd,novosRecursos)
                       
      
  }
  
    /**
   * Usada para simplesmente copiar no instante zero as tasksschedule a executar para a lista intermedia
   * colocando o conteudo das listas em - Lista intermedia com dados, lista tasksschedule a executar vazia (Nil)
   * Usada na função "definirEscalonamento"
   * 
   * @param ObjectosEscalonamento
   * @return ObjectosEscalonamento alterado
   */
  def copiarTaskExecutarToIntermedia(objEscalonamento:ObjectosEscalonamento):ObjectosEscalonamento={
    
    if(objEscalonamento.lastEnd==0){
      
      ObjectosEscalonamento(objEscalonamento.listTaskScheParaExec,Nil,objEscalonamento.listTaskScheResultado,
                           objEscalonamento.lastEnd,objEscalonamento.recs)
      
    }else{
      
      //se a lista intermedia estiver vazia coloca o start=lastEnd em todas as tasksSchedule da lista exec
        val novaListaExec=objEscalonamento.listTaskScheParaExec.map { x => x.copy(start=objEscalonamento.lastEnd) }
        
         ObjectosEscalonamento(novaListaExec,Nil,objEscalonamento.listTaskScheResultado,
                           objEscalonamento.lastEnd,objEscalonamento.recs)
      
    }
     
    
  }
  
  /**
   * Ordenação das lista de "TasksSchedule" pela propriedade "end" com ordem decrescente.
   * Uma outra forma de fazer o mesmo é fazer o mix do "trait Ordered" e definir o método "compare"
   * 
   * @param lista de "TasksSchedule"
   * @return lista de "TasksSchedule" ordenada de acordo
   */
  def taskScheduleOrdenadaZA(tskSche:List[TasksSchedule]):List[TasksSchedule]={
    
      val sortStart=tskSche.sortWith(_.start < _.start)
      val sortEnd=sortStart.sortWith(_.end < _.end)
        
       sortEnd
    
  }
  
    /**
     * remove da "listaTotal" os elementos da "subLista", de qualquer tipo
     * é usada em várias funções
     */
    def removerDaLista(subLista:List[Any],listaTotal:List[Any]):List[Any]={
    
    		listaTotal.filterNot(subLista.contains(_))
    }   
    
   /**
    * Remover da lista  "OrdemProduto" produtos sem tasks, portanto já produzidos.
    * 
    * @param lista de "OrdemProduto" para ser verificada
    * @return lista de "OrdemProduto" depois de verificada
    */
    def removerDaListaProdsSemTasks(subLista:List[OrdemProduto]):List[OrdemProduto]={
    
    		val sublist=subLista.filterNot { x => x.prod.tasks==Nil }
    		
    		sublist
    }
    
   //recursos reduzidos 
  def recursosReduzidos(opt:List[OrdemProduto],recs:Recursos):Recursos=opt match{
    
    case Nil=>recs
    case tk::Nil=>
        					
                  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							 recursosReduzidos(Nil,recursosRed)
  							  
    case tk::tks=>						  
      
                  val phyRecsAtribuidos=tk.prod.tasks.head.numPhyRectask(recs.phys) //Lista recursos fisicos desta task
  								val humRecsAtribuidos=tk.prod.tasks.head.numRechuman(phyRecsAtribuidos, recs.hums)  //Lista de recursos humanos para os recursos fisicos desta task
  								
  								//println("Human Atribuido - "+humRecsAtribuidos.toString())
  								//reduzir a lista de recursos
  								val phyRecsAtribuidosRed=removerDaLista(phyRecsAtribuidos, recs.phys).asInstanceOf[List[PhysicalResource]]  //fazer o Cast de Any
  								val humRecsAtribuidosRed=removerDaLista(humRecsAtribuidos, recs.hums).asInstanceOf[List[HumanResource]] 	//fazer o Cast de Any
  								
  							  val recursosRed=Recursos(phyRecsAtribuidosRed,humRecsAtribuidosRed)
  							  
  							  recursosReduzidos(tks,recursosRed)
    
  }
  
  
   /**
    * Usa o "TSK_2" para retirar o numero para atribuir
    * Este numero é usado para comparar o nº das tasks 
    * 
    *  @param nome so produto
    *  @return devolve um inteiro (retira do nome)
    */
    def splitprodutoNum(s:String):Integer={
 
 		val sssplit=s.indexOf("_")
 		val num=s.slice(sssplit+1, s.length()).trim()
 	
     	try {
            //Some(num.toInt)
            num.toInt
        }catch{
            case e: NumberFormatException => 0
        }
  } 
    
     /**
     * vai comparar a TasksSchedule de entrada com a lista de TasksSchedule para verificar se
     * existe uma task anterior da mesma ordem mesmo nº produto com um end inferior
     * ao start das TasksSchedule de entrada.
     * se existir altera o start e end da TasksSchedule de entrada de acordo
     * 
     * @param TasksSchedule e List[TasksSchedule]
     * @return TasksSchedule
     */
    def verificaStartTaskAnterior(taskSche:TasksSchedule,listResultado:List[TasksSchedule]):TasksSchedule={
      
          val numTask1=splitprodutoNum(taskSche.task)
          
          if(numTask1==1){
            
              taskSche
              
          }else{
            
             
          def findtaskN(taskSche:TasksSchedule,listSchedul:List[TasksSchedule]):TasksSchedule={
    
        	      val findTaskSche=listResultado.find { x => x.order==taskSche.order && x.productNum==taskSche.productNum && splitprodutoNum(x.task)==numTask1-1}
        
       
                   def optTaskSche (optiTaskSche:Option[TasksSchedule]):TasksSchedule= optiTaskSche match{
                    		
                    					case None=>taskSche
                    					
                    					case Some(t)=>  val taskResult=t
                    					
                    											if(taskSche.start<taskResult.end){
                    											
                    													// time da task em teste
                    													val taskTime=taskSche.end - taskSche.start
                    													
                    													val novoStart=taskResult.end
                    													val novoEnd=novoStart+taskTime
                    													
                    													val novaTSche=taskSche.copy( start=novoStart, end=novoEnd)
                    													
                    													novaTSche
                    											
                    											}else{
                    											
                    												taskSche
                    											}
                    											
                    								
                    		}
                    
        			  optTaskSche(findTaskSche)
            }     
              
          findtaskN(taskSche,listResultado)
          
        }      
            
    }
  
   /**
    * verifica se o end da ultima task na lista resultado é maior que o start
    * da task a ser processada
    * 
    * @param TasksSchedule e List[TasksSchedule]
    * @return TasksSchedule
    */
   def verificaEndTaskAnterior(taskSche:TasksSchedule,objEscalonamento:ObjectosEscalonamentoV2):TasksSchedule={
     
       if(objEscalonamento.listTaskScheResultado.length>0){
         
         val listaResultadoInversa=objEscalonamento.listTaskScheResultado.reverse
         
         val taskListaLimbo=objEscalonamento.listLimbo.head
         
         val taskSchedHead=listaResultadoInversa.head
         
         val casoListaresultadoMaior1=if(taskSche.start>taskSchedHead.end && taskSchedHead.physResources!=taskSche.physResources
                  && taskSchedHead.humanResources!=taskSche.humanResources
                  && taskSche.physResources!=taskListaLimbo.physResources
                  && taskSche.humanResources!=taskListaLimbo.humanResources){
               
                          
                
                 	// time da task em teste
                  val taskTime=taskSche.end - taskSche.start
                        													
                 val novoStart=taskSchedHead.end
                 val novoEnd=novoStart+taskTime
                        													
                 val novaTSche=taskSche.copy( start=novoStart, end=novoEnd)
                 
                 novaTSche
             }else{
               
                   taskSche               
             }
         
         /*
          val casoListaresultadoIgual1=if(taskSche.start>taskSchedHead.end && taskSchedHead.physResources!=taskSche.physResources
                  && taskSchedHead.humanResources!=taskSche.humanResources){

                 	// time da task em teste
                  val taskTime=taskSche.end - taskSche.start
                        													
                 val novoStart=taskSchedHead.end
                 val novoEnd=novoStart+taskTime
                        													
                 val novaTSche=taskSche.copy( start=novoStart, end=novoEnd)
                 
                 novaTSche
             }else{
               
                   taskSche               
             }
         */
         
          //val resultadoDosCasos=if(objEscalonamento.listTaskScheResultado.length==1) casoListaresultadoIgual1 else casoListaresultadoMaior1
          
          //resultadoDosCasos
         casoListaresultadoMaior1
          
       }else{
         
         taskSche
       }
         
         
        
     
   }
    /* */
    
} //fim da classe