package isep.tap.intgproj.dataIO

import scala.xml.XML
import isep.tap.intgproj.model._
import isep.tap.intgproj.prodPlan._

class DataReaderImpXML extends DataReader{
  
 
  override def read(fname: String): Option[Production] = {
     
    val whatFile:String = System.getProperty("user.dir")+"/files/"+fname
    
    try {
            val xml=XML.loadFile(whatFile)
  
            val phyResouces=physicalResourcesList(xml)
            val TTasks=taskResourceList(xml)
            val humanRec=humanResourceList(xml)
            val productTaskref=productResourceList(xml) 
            val orderss=ordersResourceList(xml)
      
          //O conjunto da produção
          Some(Production(phyResouces,TTasks,humanRec,productTaskref,orderss))
      
    } catch {
      case e: Exception => None
    }

      
   }
  
   // Get PhysicalResource  
  def physicalResourcesList(elem:scala.xml.Elem):List[PhysicalResource]={
    
      val phResouces=elem \ "PhysicalResources"     
      
            def phyResoucesfromXML(node:scala.xml.Node):PhysicalResource={
  
            		val p_id=(node \"@id").text
            		val p_type=(node \ "@type").text
          
            		PhysicalResource(p_id,p_type)
            }
      
       //Lista de "PhysicalResource"
      (phResouces \"Physical").map(phyResoucesfromXML).toList       
    
  }
 
  // Get Tasks
  def taskResourceList(elem:scala.xml.Elem):List[Task]={
    
      val taskss=elem \ "Tasks"
      
          def tasksfromXML(node:scala.xml.Node):Task={
      
          		val p_id=(node \"@id").text
          		val p_time=(node \ "@time").text.toInt
          		val p_phRec=(node \"PhysicalResource").map(n=>nodeResource(n,"@prstype")).toList
          		Task(p_id,p_time,p_phRec)
          }
       
      //Lista de tasks
      (taskss \"Task").map(tasksfromXML).toList
  }
  
  //Get Human Resources
  def humanResourceList(elem:scala.xml.Elem):List[HumanResource]={
    
      val humanRecs=elem \ "HumanResources" 
      
       
      def humanRfromXML(node:scala.xml.Node):HumanResource={
  
      		val p_id=(node \"@id").text
      		val p_nome=(node \ "@name").text
      		val p_handlesType=(node \"Handles").map(n=>nodeResource(n,"@type")).toList
      		HumanResource(p_id,p_nome,p_handlesType)
      } 
      
      (humanRecs \"Human").map(humanRfromXML).toList
  }
  
  //Get products
  def productResourceList(elem:scala.xml.Elem):List[ProductToDo]={
    
       val productsTD=elem \ "Products"       
         
          def  productsfromXML(node:scala.xml.Node):ProductToDo={
      
          		val p_id=(node \"@id").text
          		val p_nome=(node \ "@name").text
          		val p_processtskref=(node \"Process").map(n=>nodeResource(n,"@tskref")).toList
          		ProductToDo(p_id,p_nome,p_processtskref)
          }     
    
       (productsTD \"Product").map(productsfromXML).toList
  }
  
  //Get Orders
  def ordersResourceList(elem:scala.xml.Elem):List[Order]={
    
    val orders=elem \ "Orders"
    
          
          def ordersfromXML(node:scala.xml.Node):Order={
      
          		val p_id=(node \"@id").text
          		val p_prdref=(node \ "@prdref").text
        			val p_quant=(node \ "@quantity").text.toInt
          		Order(p_id,p_prdref,p_quant)
          }
   
      (orders \"Order").map(ordersfromXML).toList
  }
  
  //Get TasksSchedule
  def tasksScheduleResourceList(elem:scala.xml.Elem, pr: List[PhysicalResource], hr: List[HumanResource]):List[TasksSchedule]={
    
       val tasksSchedule=elem \ "TaskSchedule"       
         
          def tasksSchedulefromXML(node:scala.xml.Node):TasksSchedule={
      
          		val t_order=(node \"@order").text
          		val t_productNumber=(node \ "@productNumber").text.toInt
          		val t_task=(node \"@task").text
          		val t_start=(node \ "@start").text.toInt
          		val t_end=(node \ "@end").text.toInt
          		//val t_physicalResources=(node \"PhysicalResources")
          		//t_physicalResources.map(n => physicalResourcesList(n));  
          		//val p_phRec=(node \"PhysicalResource").map(n=>physicalResourcesListSchedule(n,"@id")).toList
          		val t_physicalResources=physicalResourcesListSchedule(node, pr);
          		//physicalResourcesList(t_physicalResources)
          		val t_humanResources=humanResourcesListSchedule(node, hr)
          		TasksSchedule(t_order,t_productNumber,t_task,t_start,t_end,t_physicalResources,t_humanResources)
          }     
    
       tasksSchedule.map(tasksSchedulefromXML).toList
  }
  
  // Get PhysicalResource from Schedule
  def physicalResourcesListSchedule(elem:scala.xml.Node, pr: List[PhysicalResource]):List[PhysicalResource]={
    
      //val phResouces=elem \ "PhysicalResources" 
      //val tasksSchedule=elem \ "TaskSchedule" 
      
            def phyResoucesfromXML(node:scala.xml.Node):PhysicalResource={
  
            		val p_id=(node \"@id").text
            		pr.find(p => p.id == p_id).get
            }
      
       //Lista de "PhysicalResource"
      (elem \\"Physical").map(phyResoucesfromXML).toList       
    
  }
  // Get HumanResource from Schedule
  def humanResourcesListSchedule(elem:scala.xml.Node, hr: List[HumanResource]):List[HumanResource]={
    
      //val phResouces=elem \ "PhysicalResources" 
      //val tasksSchedule=elem \ "TaskSchedule" 
      
            def humanResoucesfromXML(node:scala.xml.Node):HumanResource={
  
            		val h_name=(node \"@name").text
            		hr.find(h => h.name == h_name).get
            }
      
       //Lista de "PhysicalResource"
      (elem \\"Human").map(humanResoucesfromXML).toList       
    
  }
 //----------------------------------------------------------------------------------------------------------------
      //varios sub nos - função usada para retirar sub nos 
      //para todas as funções em cima
       def nodeResource(node:scala.xml.Node,prop:String):String={
  
  			  (node \ prop).text
  
  	  } 
        
     
           


      
      
}