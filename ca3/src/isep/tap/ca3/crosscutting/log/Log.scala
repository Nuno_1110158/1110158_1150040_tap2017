package isep.tap.ca3.crosscutting.log

import isep.tap.ca3.logger.TimeTagLogger

/*
 * TODO: create the Log trait
 */
trait Log {
  def log(message: String)
  def top(): (String, String)
}