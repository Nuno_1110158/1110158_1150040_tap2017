package isep.tap.intgproj.dataIO

trait DataReader {
  
  def read(fname: String): Option[Production]
  
}